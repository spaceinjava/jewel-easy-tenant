/**
 * 
 */
package com.spaceinje.tenant.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.spaceinje.tenant.constants.TenantConstants;

/**
 * @author Lakshmi Kiran
 *
 */
@Configuration
public class AWSConfig {

	private static final Logger log = LoggerFactory.getLogger(AWSConfig.class);

	@Autowired
	private TenantConstants constants;

	@Bean
	public AmazonS3 amazonS3Client() {
		log.info("configuring aws basic credentials");
		final BasicAWSCredentials credentials = new BasicAWSCredentials(constants.getAws_access_key(),
				constants.getAws_secret_key());
		log.info("building the aws client");
		return AmazonS3ClientBuilder.standard().withRegion(Regions.AP_SOUTH_1)
				.withCredentials(new AWSStaticCredentialsProvider(credentials)).build();
	}
}
