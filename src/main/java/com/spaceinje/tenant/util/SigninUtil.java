/**
 * 
 */
package com.spaceinje.tenant.util;

/**
 * @author Lakshmi Kiran
 * @implNote used as login request for tenant and employee
 * @version 1.0
 */
public class SigninUtil {

	private String user_name;
	
	private String password;

	/**
	 * @return the user_name
	 */
	public String getUser_name() {
		return user_name;
	}

	/**
	 * @param user_name the user_name to set
	 */
	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String toString() {
		return "SigninUtil [user_name=" + user_name + ", password=" + password + "]";
	}
	
	
}
