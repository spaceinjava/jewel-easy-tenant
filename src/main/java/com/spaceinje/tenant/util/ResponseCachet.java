/**
 * 
 */
package com.spaceinje.tenant.util;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * @author Lakshmi Kiran
 * 
 * @implSpec status which contains authorization and authentication tokens along
 * with their expire time and roles
 *
 * @version 1.0
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ResponseCachet<T> {
	
	@JsonInclude(Include.NON_NULL)
	private String status;
	
	@JsonInclude(Include.NON_NULL)
	private String message;
	
	@JsonInclude(Include.NON_NULL)
	private String traceId;
	
	@JsonInclude(Include.NON_NULL)	
	private T data;
	
	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}

	public String getTraceId() {
		return traceId;
	}

	public void setTraceId(String traceId) {
		this.traceId = traceId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	@Override
	public String toString() {
		return "ResponseCachet [status=" + status + ", message=" + message + ", traceId=" + traceId + ", data=" + data
				+ "]";
	}
}
