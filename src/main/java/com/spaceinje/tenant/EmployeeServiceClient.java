package com.spaceinje.tenant;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


import com.spaceinje.tenant.bean.UserInfoBean;
import com.spaceinje.tenant.util.ResponseCachet;

@FeignClient(name = "gateway", configuration = FeignClientConfiguration.class)
public interface EmployeeServiceClient {

	@SuppressWarnings("rawtypes")
	@RequestMapping(method = RequestMethod.POST, value = "/v1/gateway/user")
	ResponseCachet setUserInfoDetails(@RequestBody UserInfoBean bean );
}
