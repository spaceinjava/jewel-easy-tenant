
package com.spaceinje.tenant.validator;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.spaceinje.tenant.bean.BranchBean;
import com.spaceinje.tenant.model.Branch;
import com.spaceinje.tenant.repository.BranchRepository;
import com.spaceinje.tenant.specification.CommonSpecs;
import com.spaceinje.tenant.util.ResponseCachet;


@Component
public class BranchValidator extends BasicValidator {

	private static final Logger log = LoggerFactory.getLogger(BranchValidator.class);
	
	@Autowired
	private BranchRepository branchRepository;
	
	
	/**
	 * service to validate branch request
	 */
	@SuppressWarnings("unchecked")
	public ResponseCachet<List<String>> validateBranch(BranchBean bean) {
			log.debug("Entering validateBranch method");

		ResponseCachet<List<String>> cachet = new ResponseCachet<>();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		
		List<String> errors = new ArrayList<>();
		if(bean.getIsHeadBranch() && branchRepository.existsByTenantCodeAndIsHeadBranchTrue(bean.getTenantCode())) {
			log.debug("Head branch already exists");
			errors.add("Head branch already available");
			cachet.setData(errors);
			return cachet;
		}
		Optional<Branch> branch = branchRepository.findOne(CommonSpecs.findByTenantAndShortCodeSpec(bean.getTenantCode(), bean.getShortCode()));
		if (branch.isPresent() && !branch.get().getId().equals(bean.getId())) {
			errors.add("Short code already exists");
		}
		cachet.setData(errors);
		
		return cachet;
	}
}
