package com.spaceinje.tenant.validator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.spaceinje.tenant.bean.DesignationBean;
import com.spaceinje.tenant.constants.TenantConstants;
import com.spaceinje.tenant.util.ResponseCachet;

@SuppressWarnings("rawtypes")
@Component
public class TenantValidator extends BasicValidator {

	private static final Logger log = LoggerFactory.getLogger(TenantValidator.class);

	@Autowired
	private TenantConstants constants;

	public ResponseEntity<ResponseCachet> designationValidator(DesignationBean bean) {
		ResponseCachet cachet = new ResponseCachet();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		if (StringUtils.isEmpty(bean.getDepartment().getId())) {
			log.debug("department can't be empty");
			cachet.setStatus(constants.getStatus_message_failure());
			cachet.setMessage("department can't be empty");
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_ACCEPTABLE);
		}
		cachet.setStatus(constants.getStatus_message_success());
		cachet.setMessage("designation validated successfully");
		log.debug("Validation Successfull");
		return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.OK);
	}

//	
//	/**
//	 * method to validate tenant sign in utils
//	 * 
//	 * @param util - SigninUtil
//	 * 
//	 * @return {@value ResponseEntity<ResponseCachet>}
//	 */
//	public ResponseEntity<ResponseCachet> validateSignInUtils(SigninUtil util) {
//		if (log.isDebugEnabled()) {
//			log.debug("Entering validateSignInUtils method");
//		}
//		ResponseCachet cachet = new ResponseCachet();
//		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
//				.replace("LazySpan(", ""));
//		if (null == util) {
//			log.info("request can't be null");
//			cachet.setStatus(constants.getStatus_message_failure());
//			cachet.setStatus("request can't be null");
//			if (log.isDebugEnabled())
//				log.debug("Exiting validateSignInUtils method");
//			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_IMPLEMENTED);
//		}
//		if (StringUtils.isEmpty(util.getUser_name())) {
//			log.info("user name can't be empty");
//			cachet.setStatus(constants.getStatus_message_failure());
//			cachet.setStatus("user name can't be empty");
//			if (log.isDebugEnabled())
//				log.debug("Exiting validateSignInUtils method");
//			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_IMPLEMENTED);
//		}
//		if (StringUtils.isEmpty(util.getPassword())) {
//			log.info("password can't be empty");
//			cachet.setStatus(constants.getStatus_message_failure());
//			cachet.setStatus("password can't be empty");
//			if (log.isDebugEnabled())
//				log.debug("Exiting validateSignInUtils method");
//			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_IMPLEMENTED);
//		}
//		cachet.setStatus(constants.getStatus_message_success());
//		cachet.setStatus("signin utils validated successfully");
//		if (log.isDebugEnabled())
//			log.debug("Exiting validateSignInUtils method");
//		return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.OK);
//	}
//
//	/**
//	 * method to validate designation request params
//	 * 
//	 * @param bean      - DesignationBean
//	 * 
//	 * @param is_update - to check whether the method is called from update service
//	 *                  or not
//	 * 
//	 * @return {@value ResponseEntity<ResponseCachet>}
//	 */
//	public ResponseEntity<ResponseCachet> designationValidator(DesignationBean bean, boolean is_update) {
//		if (log.isDebugEnabled()) {
//			log.debug("Entering designationValidator method");
//		}
//		ResponseCachet cachet = new ResponseCachet();
//		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
//				.replace("LazySpan(", ""));
//		if (null == bean) {
//			log.info("request can't be null");
//			cachet.setStatus(constants.getStatus_message_failure());
//			cachet.setStatus("request can't be null");
//			if (log.isDebugEnabled())
//				log.debug("Exiting designationValidator method");
//			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_ACCEPTABLE);
//		}
//		if (StringUtils.isEmpty(bean.getTenant_code())) {
//			log.info("tenant code can't be wmpty");
//			cachet.setStatus(constants.getStatus_message_failure());
//			cachet.setStatus("tenant code can't be wmpty");
//			if (log.isDebugEnabled())
//				log.debug("Exiting designationValidator method");
//			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_ACCEPTABLE);
//		}
//		if (StringUtils.isEmpty(bean.getDepartment_name())) {
//			log.info("invalid department name");
//			cachet.setStatus(constants.getStatus_message_failure());
//			cachet.setStatus("invalid department name");
//			if (log.isDebugEnabled())
//				log.debug("Exiting departmentValidator method");
//			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_ACCEPTABLE);
//		}
//		if (StringUtils.isEmpty(bean.getDesignation_name())) {
//			log.info("invalid designation name");
//			cachet.setStatus(constants.getStatus_message_failure());
//			cachet.setStatus("invalid designation name");
//			if (log.isDebugEnabled())
//				log.debug("Exiting designationValidator method");
//			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_ACCEPTABLE);
//		}
//		if (StringUtils.isEmpty(bean.getShort_code())) {
//			log.info("invalid designation short code");
//			cachet.setStatus(constants.getStatus_message_failure());
//			cachet.setStatus("invalid designation short code");
//			if (log.isDebugEnabled())
//				log.debug("Exiting designationValidator method");
//			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_ACCEPTABLE);
//		}
//		if (is_update) {
//			if (StringUtils.isEmpty(bean.getId())) {
//				log.info("invalid designation id");
//				cachet.setStatus(constants.getStatus_message_failure());
//				cachet.setStatus("invalid designation id");
//				if (log.isDebugEnabled())
//					log.debug("Exiting designationValidator method");
//				return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_ACCEPTABLE);
//			}
//		}
//		cachet.setStatus(constants.getStatus_message_success());
//		cachet.setStatus("designation params validated successfully");
//		if (log.isDebugEnabled())
//			log.debug("Exiting designationValidator method");
//		return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.OK);
//	}
//
//	/**
//	 * method to validate department request params
//	 * 
//	 * @param bean      - DepartmentBean
//	 * 
//	 * @param is_update - to check whether the method is called from update service
//	 *                  or not
//	 * 
//	 * @return {@value ResponseEntity<ResponseCachet>}
//	 */
//	public ResponseEntity<ResponseCachet> departmentValidator(DepartmentBean bean, boolean is_update) {
//		if (log.isDebugEnabled()) {
//			log.debug("Entering departmentValidator method");
//		}
//		ResponseCachet cachet = new ResponseCachet();
//		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
//				.replace("LazySpan(", ""));
//		if (null == bean) {
//			log.info("request can't be null");
//			cachet.setStatus(constants.getStatus_message_failure());
//			cachet.setStatus("request can't be null");
//			if (log.isDebugEnabled())
//				log.debug("Exiting departmentValidator method");
//			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_ACCEPTABLE);
//		}
//		if (StringUtils.isEmpty(bean.getTenant_code())) {
//			log.info("tenant code can't be wmpty");
//			cachet.setStatus(constants.getStatus_message_failure());
//			cachet.setStatus("tenant code can't be wmpty");
//			if (log.isDebugEnabled())
//				log.debug("Exiting departmentValidator method");
//			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_ACCEPTABLE);
//		}
//		if (StringUtils.isEmpty(bean.getDepartment_name())) {
//			log.info("invalid department name");
//			cachet.setStatus(constants.getStatus_message_failure());
//			cachet.setStatus("invalid department name");
//			if (log.isDebugEnabled())
//				log.debug("Exiting departmentValidator method");
//			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_ACCEPTABLE);
//		}
//		if (StringUtils.isEmpty(bean.getShort_code())) {
//			log.info("invalid department short code");
//			cachet.setStatus(constants.getStatus_message_failure());
//			cachet.setStatus("invalid department short code");
//			if (log.isDebugEnabled())
//				log.debug("Exiting departmentValidator method");
//			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_ACCEPTABLE);
//		}
//		if (is_update) {
//			if (StringUtils.isEmpty(bean.getId())) {
//				log.info("invalid department id");
//				cachet.setStatus(constants.getStatus_message_failure());
//				cachet.setStatus("invalid department id");
//				if (log.isDebugEnabled())
//					log.debug("Exiting designationValidator method");
//				return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_ACCEPTABLE);
//			}
//		}
//		cachet.setStatus(constants.getStatus_message_success());
//		cachet.setStatus("designation params validated successfully");
//		if (log.isDebugEnabled())
//			log.debug("Exiting designationValidator method");
//		return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.OK);
//	}

}
