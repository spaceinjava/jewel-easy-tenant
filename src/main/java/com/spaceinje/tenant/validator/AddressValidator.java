///**
// * 
// */
//package com.spaceinje.tenant.validator;
//
//import java.util.ArrayList;
//import java.util.List;
//
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.stereotype.Component;
//import org.springframework.util.StringUtils;
//
//import com.spaceinje.tenant.bean.AddressBean;
//import com.spaceinje.tenant.util.ResponseCachet;
//
///**
// * @author Lakshmi Kiran
// *
// * @implNote validates the address request and response
// * 
// * @version 1.0
// */
//@Component
//public class AddressValidator extends BasicValidator{
//
//	private static final Logger log = LoggerFactory.getLogger(AddressValidator.class);
//	
//	/**
//	 * service to validate address request
//	 * 
//	 * @param bean -  AddressBean
//	 * 
//	 * @param request_type - save or update
//	 * 
//	 * @return ResponseCachet
//	 */
//	public ResponseCachet<List<String>> validateAddress(AddressBean bean, int request_type) {
//		if(log.isDebugEnabled())
//			log.debug("Entering validateAddress service");
//		ResponseCachet<List<String>> cachet = new ResponseCachet<List<String>>();
//		List<String> errors = new ArrayList<String>();
//		log.info("validating address");
//		
//		if(null == bean) {
//			log.info("request can't be empty");
//			cachet.setStatus(constants.getStatus_message_failure());
//			cachet.setMessage("request can't be empty");
//			
//			if(log.isDebugEnabled())
//				log.debug("Exiting validateAddress service");
//			return cachet;
//		}
//		
//		if(request_type == constants.getRequest_type_update()) {
//			if(StringUtils.isEmpty(bean.getId())) {
//				log.info("Invalid address id");
//				errors.add("Invalid address id");
//			}
//		}
//		if(StringUtils.isEmpty(bean.getAddress_line_1())) {
//			log.info("address line 1 can't be empty");
//			errors.add("address line 1 can't be empty");
//		}
//		if(bean.getPostal() == null) {
//			log.info("postal info can't be empty");
//			errors.add("postal info can't be empty");
//		} else {
//			if(StringUtils.isEmpty(bean.getPostal().getId())) {
//				log.info("Invalid postal Id");
//				errors.add("Invalid postal Id");
//			}
//		}
//		
//		log.info("validating messages");
//		if(errors.size() > 0) {
//			log.info("Invalid Address request");
//			cachet.setStatus(constants.getStatus_message_failure());
//			cachet.setMessage("Invalid Address request");
//			cachet.setData(errors);
//			
//			if(log.isDebugEnabled())
//				log.debug("Exiting validateAddress service");
//			return cachet;
//		}
//		log.info("Address request valid");
//		cachet.setStatus(constants.getStatus_message_success());
//		cachet.setMessage("Address request valid");
//		
//		if(log.isDebugEnabled())
//			log.debug("Exiting validateAddress service");
//		return cachet;
//	}
//}
