///**
// * 
// */
//package com.spaceinje.tenant.validator;
//
//import java.util.ArrayList;
//import java.util.List;
//
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Component;
//import org.springframework.util.CollectionUtils;
//import org.springframework.util.StringUtils;
//
//import com.spaceinje.tenant.bean.CompanyBean;
//import com.spaceinje.tenant.util.ResponseCachet;
//
///**
// * @author Lakshmi Kiran
// *
// */
//@Component
//public class CompanyValidator extends BasicValidator {
//
//	private static final Logger log = LoggerFactory.getLogger(CompanyValidator.class);
//	
//	@Autowired
//	private AddressValidator addressValidator;
//	
//	/**
//	 * service to validate company request
//	 */
//	public ResponseCachet<List<String>> validateCompany(CompanyBean bean, int request_type) {
//		if (log.isDebugEnabled())
//			log.debug("Entering validateCompany method");
//
//		ResponseCachet<List<String>> cachet = new ResponseCachet<>();
//		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
//				.replace("LazySpan(", ""));
//		List<String> errors = new ArrayList<String>();
//		
//		log.info("validating bean");
//		if(null == bean) {
//			log.info("request can't be empty");
//			cachet.setStatus(constants.getStatus_message_failure());
//			cachet.setMessage("request can't be empty");
//			
//			if(log.isDebugEnabled())
//				log.debug("Exiting validateCompany service");
//			return cachet;
//		}
//		
//		log.info("validating address");
//		cachet = addressValidator.validateAddress(bean.getAddress(), request_type);
//		if (cachet.getStatus().equals(constants.getStatus_message_failure())) {
//			log.info("error in the address request");
//			if (!CollectionUtils.isEmpty(cachet.getData())) {
//				errors.addAll((List<String>) cachet.getData());
//			} else {
//				errors.add(cachet.getMessage());
//			}
//		}
//		
//		log.info("validating alternate contact");
//		if(StringUtils.isEmpty(bean.getAlternate_concact())) {
//			log.info("alternate contact can't be null");
//			errors.add("alternate contact can't be null");
//		} else {
//			if(!isMobileValid(bean.getAlternate_concact())) {
//				log.info("invalid alternate contact number");
//				errors.add("invalid alternate contact number");
//			}
//		}
//		
//		log.info("validating company name");
//		if(StringUtils.isEmpty(bean.getCompany_name())) {
//			log.info("company name can't be null");
//			errors.add("company name can't be null");
//		}
//		
//		log.info("validating contact");
//		if(StringUtils.isEmpty(bean.getContact())){
//			log.info("contact can't be null");
//			errors.add("contact can't be null");
//		} else {
//			if(!isMobileValid(bean.getContact())) {
//				log.info("invalid contact number");
//				errors.add("invalid contact number");
//			}
//		}
//		
//		log.info("validating email");
//		if(StringUtils.isEmpty(bean.getEmail())) {
//			log.info("email can't be null");
//			errors.add("email can't be null");
//		} else {
//			if(!isValidEmail(bean.getEmail())) {
//				log.info("invalid email");
//				errors.add("invalid email");
//			}
//		}
//		
//		
//		log.info("validating company id");
//		if(request_type == constants.getRequest_type_update()) {
//			if(StringUtils.isEmpty(bean.getId())) {
//				log.info("Invalid company Id");
//				errors.add("Invalid company Id");
//			}
//		}
//		
//		log.info("validating short name");
//		if(StringUtils.isEmpty(bean.getShort_name())) {
//			log.info("short name can't be null");
//			errors.add("short name can't be null");
//		}
//		
//		log.info("validating messages");
//		if(errors.size() > 0) {
//			log.info("Invalid company request");
//			cachet.setStatus(constants.getStatus_message_failure());
//			cachet.setMessage("Invalid Address request");
//			cachet.setData(errors);
//			
//			if(log.isDebugEnabled())
//				log.debug("Exiting validateCompany service");
//			return cachet;
//		}
//		log.info("Company request valid");
//		cachet.setStatus(constants.getStatus_message_success());
//		cachet.setMessage("Company request valid");
//		
//		if(log.isDebugEnabled())
//			log.debug("Exiting validateCompany service");
//		return cachet;
//	}
//}
