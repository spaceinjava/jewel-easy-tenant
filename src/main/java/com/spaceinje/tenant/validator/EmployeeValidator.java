
package com.spaceinje.tenant.validator;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.spaceinje.tenant.bean.EmployeeBean;
import com.spaceinje.tenant.model.Employee;
import com.spaceinje.tenant.repository.EmployeeRepository;
import com.spaceinje.tenant.util.ResponseCachet;

@Component("employeeValidator")
public class EmployeeValidator extends BasicValidator {

	private static final Logger log = LoggerFactory.getLogger(EmployeeValidator.class);

	@Autowired
	private EmployeeRepository employeeRepository;

	/**
	 * method to validate the employee request
	 * 
	 * @param bean         - EmployeeBean
	 * 
	 * @return {@value ResponseCachet<List<String>>}
	 */
	public ResponseCachet<List<String>> validateEmployee(EmployeeBean bean) {
		log.debug("Entering validateEmployee method");
		ResponseCachet<List<String>> cachet = new ResponseCachet<>();
		String traceId = tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", "");
		List<String> errors = new ArrayList<String>();
		cachet.setTraceId(traceId);
		
		Optional<Employee> emp = employeeRepository.findByTenantCodeAndEmployeeId(bean.getTenantCode(),bean.getEmployeeId());

		if (emp.isPresent() && !emp.get().getId().equals(bean.getId())) {
			errors.add("Employee Id is already present");
		}
		Optional<Employee> empWithPh = employeeRepository.findByMobileOrEmailAndTenantCode(bean.getMobile(), bean.getEmail(), bean.getTenantCode());
		if (empWithPh.isPresent() && !empWithPh.get().getId().equals(bean.getId())) {
			errors.add("Mobile Or Email is not Unique");
		}
		cachet.setData(errors);
		return cachet;

	}

}
