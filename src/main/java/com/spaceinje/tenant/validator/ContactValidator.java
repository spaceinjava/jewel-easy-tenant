///**
// * 
// */
//package com.spaceinje.tenant.validator;
//
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.stereotype.Component;
//import org.springframework.util.StringUtils;
//
//import com.spaceinje.tenant.bean.ContactBean;
//import com.spaceinje.tenant.util.ResponseCachet;
//
///**
// * @author Lakshmi Kiran
// * @implNote class which validates contact request
// * @version 1.0
// */
//@Component("contactValidator")
//public class ContactValidator extends BasicValidator {
//
//	private static final Logger log = LoggerFactory.getLogger(ContactValidator.class);
//
//	@SuppressWarnings("rawtypes")
//	public ResponseCachet isContactValid(ContactBean bean, int request_type) {
//		if (log.isDebugEnabled())
//			log.debug("Entering isContactValid method");
//		ResponseCachet cachet = new ResponseCachet<>();
//		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
//				.replace("LazySpan(", ""));
//
//		if (null == bean) {
//			log.info("request can't be null");
//			cachet.setStatus(constants.getStatus_message_failure());
//			cachet.setStatus("request can't be null");
//			if (log.isDebugEnabled())
//				log.debug("Exiting isContactValid method");
//			return cachet;
//		}
//		if (StringUtils.isEmpty(bean.getAadhar())) {
//			log.info("aadhar can't be null");
//			cachet.setStatus(constants.getStatus_message_failure());
//			cachet.setStatus("aadhar can't be null");
//			if (log.isDebugEnabled())
//				log.debug("Exiting isContactValid method");
//			return cachet;
//		} else {
//			if (!isAadharValid(bean.getAadhar())) {
//				log.info("invalid aadhar number");
//				cachet.setStatus(constants.getStatus_message_failure());
//				cachet.setStatus(
//						"invalid aadhar number, It should have 12 digits." + "It should not start with 0 and 1."
//								+ "It should not contain any alphabet and special characters."
//								+ "It should have white space after every 4 digits.");
//				if (log.isDebugEnabled())
//					log.debug("Exiting isContactValid method");
//				return cachet;
//			}
//		}
//
//		if (StringUtils.isEmpty(bean.getPan())) {
//			log.info("pan can't be null");
//			cachet.setStatus(constants.getStatus_message_failure());
//			cachet.setStatus("pan can't be null");
//			if (log.isDebugEnabled())
//				log.debug("Exiting isContactValid method");
//			return cachet;
//		} else {
//			if (!isPANValid(bean.getPan())) {
//				log.info("invalid pan number");
//				cachet.setStatus(constants.getStatus_message_failure());
//				cachet.setStatus("invalid pan number, It should be ten characters long.\r\n"
//						+ "The first five characters should be any upper case alphabets.\r\n"
//						+ "The next four-characters should be any number from 0 to 9.\r\n"
//						+ "The last(tenth) character should be any upper case alphabet.\r\n"
//						+ "It should not contain any white spaces.");
//				if (log.isDebugEnabled())
//					log.debug("Exiting isContactValid method");
//				return cachet;
//			}
//		}
//
//		if (StringUtils.isEmpty(bean.getContact())) {
//			log.info("contact can't be null");
//			cachet.setStatus(constants.getStatus_message_failure());
//			cachet.setStatus("contact can't be null");
//			if (log.isDebugEnabled())
//				log.debug("Exiting isContactValid method");
//			return cachet;
//		} else {
//			if (!isMobileValid(bean.getContact())) {
//				log.info("invalid contact number");
//				cachet.setStatus(constants.getStatus_message_failure());
//				cachet.setStatus(
//						"invalid contact : The first digit should contain number between 6 to 9. The rest 9\r\n"
//								+ "	 *           digit can contain any number between 0 to 9. The mobile number can\r\n"
//								+ "	 *           have 11 digits also by including 0 at the starting. The mobile\r\n"
//								+ "	 *           number can be of 12 digits also by including 91 at the starting");
//				if (log.isDebugEnabled())
//					log.debug("Exiting isContactValid method");
//				return cachet;
//			}
//		}
//
//		if (StringUtils.isEmpty(bean.getEmergency_contact())) {
//			log.info("emergency contact can't be null");
//			cachet.setStatus(constants.getStatus_message_failure());
//			cachet.setStatus("emergency contact can't be null");
//			if (log.isDebugEnabled())
//				log.debug("Exiting isContactValid method");
//			return cachet;
//		} else {
//			if (!isMobileValid(bean.getEmergency_contact())) {
//				log.info("invalid contact number");
//				cachet.setStatus(constants.getStatus_message_failure());
//				cachet.setStatus(
//						"invalid emergency contact : The first digit should contain number between 6 to 9. The rest 9\r\n"
//								+ "	 *           digit can contain any number between 0 to 9. The mobile number can\r\n"
//								+ "	 *           have 11 digits also by including 0 at the starting. The mobile\r\n"
//								+ "	 *           number can be of 12 digits also by including 91 at the starting");
//				if (log.isDebugEnabled())
//					log.debug("Exiting isContactValid method");
//				return cachet;
//			}
//		}
//
//		if (request_type == constants.getRequest_type_update()) {
//			if (StringUtils.isEmpty(bean.getId())) {
//				log.info("invalid contact id");
//				cachet.setStatus(constants.getStatus_message_failure());
//				cachet.setStatus("invalid contact id");
//				if (log.isDebugEnabled())
//					log.debug("Exiting isContactValid method");
//				return cachet;
//			}
//		}
//		cachet.setStatus(constants.getStatus_message_success());
//		cachet.setStatus("contact is valid");
//		if (log.isDebugEnabled())
//			log.debug("Exiting isContactValid method");
//		return cachet;
//	}
//}
