package com.spaceinje.tenant.repository;

import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.spaceinje.tenant.model.Department;

@Repository
public interface DepartmentRepository extends JpaRepository<Department, String>, JpaSpecificationExecutor<Department> {

	@Query(value = "select case when count(dt) > 0 then true else false end from Department dt left join dt.designations ds on ds.department = dt "
			+ " where ds.isDeleted = false and dt.id in (?1)")
	public boolean departmentHasOrphans(Set<String> departmentIds);

	@Modifying
	@Transactional
	@Query(value = "update Department set isDeleted = true where id in (?1)")
	public void deleteDepartments(Set<String> departmentIds);

}
