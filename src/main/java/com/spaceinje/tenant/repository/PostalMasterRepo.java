package com.spaceinje.tenant.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.spaceinje.tenant.model.base.Postal;

@Repository
public interface PostalMasterRepo extends JpaRepository<Postal, Integer>{

	Optional<List<Postal>> findByPincode(String pincode);


}
