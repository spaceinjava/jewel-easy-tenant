package com.spaceinje.tenant.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.spaceinje.tenant.model.base.Bank;

@Repository
public interface BankMasterRepo extends JpaRepository<Bank, String>{

}
