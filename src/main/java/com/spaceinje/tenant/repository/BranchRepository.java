package com.spaceinje.tenant.repository;

import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import com.spaceinje.tenant.model.Branch;

public interface BranchRepository extends JpaRepository<Branch, String>, JpaSpecificationExecutor<Branch>{

	@Modifying
	@Transactional
	@Query(value = "update Branch set isDeleted = true where id in (?1)")
	public void deleteBranches(Set<String> branchIds);
	
	public boolean existsByTenantCodeAndIsHeadBranchTrue(String tenantCode);
	
	public boolean existsByIdInAndIsHeadBranchTrue(Set<String> ids);
}
