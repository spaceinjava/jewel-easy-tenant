package com.spaceinje.tenant.repository;

import java.util.Optional;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import com.spaceinje.tenant.model.Employee;

public interface EmployeeRepository extends JpaRepository<Employee, String>, JpaSpecificationExecutor<Employee>{
	
	public Optional<Employee> findByMobileOrEmailAndTenantCode(String mobile, String email,String tenantCode);
	
	public Optional<Employee> findByTenantCodeAndEmployeeId(String tenantCode, String employeeId);
	
	public Optional<Employee> findTopByTenantCodeOrderByCreatedDateDesc(String tenantCode);
	
	@Modifying
	@Transactional
	@Query(value = "update Employee set isDeleted = true where id in (?1)")
	public void deleteEmployees(Set<String> empIds);
	
}
