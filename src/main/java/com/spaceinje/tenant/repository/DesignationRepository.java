package com.spaceinje.tenant.repository;

import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.spaceinje.tenant.model.Designation;

@Repository
public interface DesignationRepository extends JpaRepository<Designation, String>, JpaSpecificationExecutor<Designation> {

	@Modifying
	@Transactional
	@Query(value = "update Designation set isDeleted = true where id in (?1)")
	public void deleteDesignations(Set<String> designationIds);

}
