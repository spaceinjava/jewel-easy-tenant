package com.spaceinje.tenant.repository;

import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.spaceinje.tenant.model.Supplier;

@Repository
public interface SupplierRepository extends JpaRepository<Supplier, String>, JpaSpecificationExecutor<Supplier> {

	@Modifying
	@Transactional
	@Query(value = "update Supplier set isDeleted = true where id in (?1)")
	public void deleteSuppliers(Set<String> supplierIds);

}
