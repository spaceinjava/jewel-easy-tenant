package com.spaceinje.tenant;

import org.modelmapper.ModelMapper;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.web.client.RestTemplate;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.servers.Server;

@RefreshScope
@EnableDiscoveryClient
@SpringBootApplication
@EnableFeignClients
@ComponentScan(basePackages = "com.spaceinje.tenant")
@EnableHystrix
@OpenAPIDefinition(info = @Info(title = "Jewel Easy Tenant Service", description = "contains the services related to tenant", version = "1.0"), servers = {
		@Server(url = "https://dev.spaceinje.com/tenant", description = "Dev Environment"),
		@Server(url = "http://localhost:2000", description = "Local") })
@EnableJpaRepositories(basePackages = { "com.spaceinje.tenant.repository" })
@EnableJpaAuditing
@EnableCaching
@EnableWebSecurity
public class JewelEasyTenantApplication {

	public static void main(String[] args) {
		SpringApplication.run(JewelEasyTenantApplication.class, args);
	}

	@Bean
	public ModelMapper modelMapper() {
		return new ModelMapper();
	}

	@Bean
	public RestTemplate restTemplate() {
		return new RestTemplate();
	}

}
