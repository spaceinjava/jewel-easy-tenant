package com.spaceinje.tenant.converter;

import java.util.Set;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import com.spaceinje.tenant.bean.AddressBean;
import com.spaceinje.tenant.bean.SupplierBean;
import com.spaceinje.tenant.model.Address;
import com.spaceinje.tenant.model.BankDetails;
import com.spaceinje.tenant.model.Supplier;
import com.spaceinje.tenant.model.SupplierDetails;

@Service
public class SupplierConverter {

	ModelMapper mapper = new ModelMapper();

	public Supplier beanToEntity(SupplierBean bean) {
		Supplier entity = mapper.map(bean, Supplier.class);
		Address address = mapper.map(bean.getAddress(),Address.class);
		Set<BankDetails> setBankDetails = bean.getBankDetails().stream().map(ele -> mapper.map(ele, BankDetails.class)).collect(Collectors.toSet());
		Set<SupplierDetails> setSupplierDetails = bean.getSupplierDetails().stream().map(ele -> mapper.map(ele, SupplierDetails.class)).collect(Collectors.toSet());
		entity.setBankDetails(setBankDetails);
		entity.setAddress(address);
		entity.setSupplierDetails(setSupplierDetails);
		return entity;
	}

	public SupplierBean entityToBean(Supplier entity) {
		SupplierBean bean = mapper.map(entity, SupplierBean.class);
		AddressBean addressBean = mapper.map(entity.getAddress(), AddressBean.class);
		bean.setAddress(addressBean);
		return bean;
	}
}
