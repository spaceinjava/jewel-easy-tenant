package com.spaceinje.tenant.converter;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import com.spaceinje.tenant.bean.DepartmentBean;
import com.spaceinje.tenant.bean.DesignationBean;
import com.spaceinje.tenant.model.Department;
import com.spaceinje.tenant.model.Designation;

@Service
public class DesignationConverter {

	ModelMapper mapper = new ModelMapper();

	public Designation beanToEntity(DesignationBean bean) {
		Designation entity = mapper.map(bean, Designation.class);
		DepartmentBean departmentBean = bean.getDepartment();
		Department department = mapper.map(departmentBean, Department.class);
		entity.setDepartment(department);
		return entity;
	}

	public DesignationBean entityToBean(Designation entity) {
		DesignationBean bean = mapper.map(entity, DesignationBean.class);
		Department department = entity.getDepartment();
		DepartmentBean departmentBean = mapper.map(department, DepartmentBean.class);
		bean.setDepartment(departmentBean);
		return bean;
	}
}
