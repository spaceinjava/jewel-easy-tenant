package com.spaceinje.tenant.converter;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;

import com.spaceinje.tenant.bean.EmployeeBean;
import com.spaceinje.tenant.model.Employee;

public class EmployeeConverter {
	
	@Autowired
	private ModelMapper mapper;
	
	public Employee beanToEntity(EmployeeBean bean) {
		
		Employee emp = mapper.map(bean, Employee.class);
		
		return emp;
		
	}

}
