package com.spaceinje.tenant.service;

import java.util.Set;

import com.spaceinje.tenant.bean.DesignationBean;
import com.spaceinje.tenant.util.ResponseCachet;

@SuppressWarnings("rawtypes")
public interface DesignationService {

	public ResponseCachet saveOrUpdateDesignation(DesignationBean bean);
	public ResponseCachet fetchAllDesignations(String tenantCode);
	public ResponseCachet deleteDesignations(Set<String> designationIds);

}
