package com.spaceinje.tenant.service;

import java.util.Set;

import org.springframework.web.multipart.MultipartFile;

import com.spaceinje.tenant.bean.SupplierBean;
import com.spaceinje.tenant.util.ResponseCachet;

@SuppressWarnings("rawtypes")
public interface SupplierService {

	public ResponseCachet fetchAllSuppliers(String tenantCode);

	public ResponseCachet deleteSuppliers(Set<String> supplierIds);

	public ResponseCachet saveOrUpdateSupplier(SupplierBean bean, MultipartFile file);

}
