package com.spaceinje.tenant.service;

import java.util.List;
import java.util.Set;

import com.spaceinje.tenant.bean.BranchBean;
import com.spaceinje.tenant.util.ResponseCachet;

public interface BranchService {

	ResponseCachet<BranchBean> saveBranch(BranchBean bean);
	
	ResponseCachet<List<BranchBean>> fetchAllBranches(String tenantCode);
	
	ResponseCachet<?> deleteBranches(Set<String> ids);
	
	
}
