package com.spaceinje.tenant.service;

import java.util.List;

import com.spaceinje.tenant.bean.base.BankBean;
import com.spaceinje.tenant.bean.base.PostalBean;
import com.spaceinje.tenant.util.ResponseCachet;

public interface MasterDataService {

	ResponseCachet<BankBean> getBankDetailsByIfsc(String ifsc);

	ResponseCachet<List<PostalBean>> getPostalDetailsByPincode(String pincode);

}