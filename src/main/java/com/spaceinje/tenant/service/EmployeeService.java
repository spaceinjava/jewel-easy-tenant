package com.spaceinje.tenant.service;

import java.util.List;
import java.util.Set;

import org.springframework.web.multipart.MultipartFile;

import com.spaceinje.tenant.bean.EmployeeBean;
import com.spaceinje.tenant.util.ResponseCachet;

public interface EmployeeService {

	public ResponseCachet<EmployeeBean> saveOrUpdateEmployee(EmployeeBean bean, MultipartFile file);

	public ResponseCachet<?> deleteEmployees(Set<String> ids);

	public ResponseCachet<List<EmployeeBean>> fetchAllEmployees(String tenantCode);

	public ResponseCachet<List<EmployeeBean>> getAllEmployeesByIsLogin(String tenant_code);

}
