package com.spaceinje.tenant.service;

import java.util.Set;

import com.spaceinje.tenant.bean.DepartmentBean;
import com.spaceinje.tenant.util.ResponseCachet;

@SuppressWarnings("rawtypes")
public interface DepartmentService {
	
	public ResponseCachet saveOrUpdateDepartment(DepartmentBean bean);
	public ResponseCachet fetchAllDepartments(String tenantCode);
	public ResponseCachet deleteDepartments(Set<String> departmentIds);
	
}
