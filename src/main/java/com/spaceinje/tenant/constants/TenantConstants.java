/**
 * 
 */
package com.spaceinje.tenant.constants;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @author Lakshmi Kiran
 * @implNote specifies the common constants used by all the services
 * @version 1.0
 */
@Component("constants")
public class TenantConstants {
	
	String ternary = "?";
	
	String colon = ":";
	
	String single_slash = "/";
	
	String double_slash = "//";
	
	String and = "&";
	
	String equals = "=";
	
	int request_type_save = 0;
	
	int request_type_update = 1;
	
	@Value("${status.message.success}")
	String status_message_success;
	
	@Value("${status.message.failure}")
	String status_message_failure;
	
	@Value("${status.message.exception}")
	String status_message_exception;
	
	@Value("${validate.params.request.token.key}")
	String token_key;
	
	@Value("${validate.params.request.tenant.hash.key}")
	String tenant_hash_key;
	
	@Value("${validate.params.request.secret.key}")
	String secret_key;
	
	@Value("${validate.params.request.code.key}")
	String code_key;
	
	@Value("${service.protocol}")
	String protocol;
	
	@Value("${application.gateway}")
	String app_gateway;

	@Value("${validate.params.mapping}")
	String validate_mapping_params;
	
	@Value("${authentication.mapping}")
	String authentication_mapping;
	
	@Value("${email.regex}")
	String email_regex;
	
	@Value("${mobile.regex}")
	String mobile_regex;
	
	@Value("${password.regex}")
	String password_regex;
	
	@Value("${pincode.regex}")
	String pincode_regex;
	
	@Value("${name.regex}")
	String name_regex;
	
	@Value("${secret.key.salt.algo}")
	String secret_key_salt_algo;

	@Value("${max.login.attempts}")
	String max_login_attempts;
	
	@Value("${session.validation.mapping}")
	String session_validation_mapping;
	
	@Value("${logout.mapping}")
	String logout_mapping;
	
	@Value("${validate.params.request.auth.token.key}")
	String auth_token_key;
	
	@Value("${service.name.key}")
	String service_name_key;
	
	@Value("${tenant.password.suffix}")
	String tenant_password_suffix;
	
	@Value("${password.reset.url}")
	String password_reset_url;
	
	@Value("${spring.security.user.name}")
	String username;
	
	@Value("${spring.security.user.password}")
	String password;
	
	@Value("${aadhar.regex}")
	String aadhar_regex;
	
	@Value("${pan.regex}")
	String pan_regex;
	
	@Value("${postal.url}")
	String postal_url;
	
	@Value("${ifsc.url}")
	String ifsc_url;
	
	@Value("${account.number.regex}")
	String account_number_regex;
	
//	@Value("${response.type.key}")
//	String response_type_key;
	
	@Value("${emp.code.prefix}")
	String emp_code_prefix;
	
	@Value("${id.length}")
	int id_length;
	
	@Value("${aws.access.key}")
	String aws_access_key;
	
	@Value("${aws.secret.key}")
	String aws_secret_key;
	
	@Value("${aws.s3.bucket.name}")
	String aws_s3_bucket_name;
	
	@Value("${aws.s3.bucket.url}")
	String aws_s3_bucket_url;
	
	@Value("${employee.temporary.password}")
	String employeeTemporaryPassword;
	
	public String getAws_s3_bucket_url() {
		return aws_s3_bucket_url;
	}

	public String getTernary() {
		return ternary;
	}

	public String getColon() {
		return colon;
	}

	public String getSingle_slash() {
		return single_slash;
	}

	public String getDouble_slash() {
		return double_slash;
	}

	public String getAnd() {
		return and;
	}

	public String getEquals() {
		return equals;
	}

	public int getRequest_type_save() {
		return request_type_save;
	}

	public int getRequest_type_update() {
		return request_type_update;
	}

	public String getStatus_message_success() {
		return status_message_success;
	}

	public String getStatus_message_failure() {
		return status_message_failure;
	}

	public String getStatus_message_exception() {
		return status_message_exception;
	}

	public String getToken_key() {
		return token_key;
	}

	public String getTenant_hash_key() {
		return tenant_hash_key;
	}

	public String getSecret_key() {
		return secret_key;
	}

	public String getCode_key() {
		return code_key;
	}

	public String getProtocol() {
		return protocol;
	}

	public String getApp_gateway() {
		return app_gateway;
	}

	public String getValidate_mapping_params() {
		return validate_mapping_params;
	}

	public String getAuthentication_mapping() {
		return authentication_mapping;
	}

	public String getEmail_regex() {
		return email_regex;
	}

	public String getMobile_regex() {
		return mobile_regex;
	}

	public String getPassword_regex() {
		return password_regex;
	}

	public String getPincode_regex() {
		return pincode_regex;
	}

	public String getName_regex() {
		return name_regex;
	}

	public String getSecret_key_salt_algo() {
		return secret_key_salt_algo;
	}

	public String getMax_login_attempts() {
		return max_login_attempts;
	}

	public String getSession_validation_mapping() {
		return session_validation_mapping;
	}

	public String getLogout_mapping() {
		return logout_mapping;
	}

	public String getAuth_token_key() {
		return auth_token_key;
	}

	public String getService_name_key() {
		return service_name_key;
	}

	public String getTenant_password_suffix() {
		return tenant_password_suffix;
	}

	public String getPassword_reset_url() {
		return password_reset_url;
	}

	public String getUsername() {
		return username;
	}

	public String getPassword() {
		return password;
	}

	public String getAadhar_regex() {
		return aadhar_regex;
	}

	public String getPan_regex() {
		return pan_regex;
	}

	public String getPostal_url() {
		return postal_url;
	}

	public String getIfsc_url() {
		return ifsc_url;
	}

	public String getAccount_number_regex() {
		return account_number_regex;
	}

//	public String getResponse_type_key() {
//		return response_type_key;
//	}

	public String getEmp_code_prefix() {
		return emp_code_prefix;
	}

	public int getId_length() {
		return id_length;
	}

	public String getAws_access_key() {
		return aws_access_key;
	}

	public String getAws_secret_key() {
		return aws_secret_key;
	}

	public String getAws_s3_bucket_name() {
		return aws_s3_bucket_name;
	}

	public String getEmployeeTemporaryPassword() {
		return employeeTemporaryPassword;
	}
	
}
