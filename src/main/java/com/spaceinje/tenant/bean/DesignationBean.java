package com.spaceinje.tenant.bean;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import com.spaceinje.tenant.bean.base.BaseBean;

public class DesignationBean extends BaseBean {

	@NotEmpty(message = "tenant code can't be empty")
	private String tenantCode;

	@NotEmpty(message = "designation name can't be empty")
	private String designationName;

	@NotEmpty(message = "short code can't be empty")
	private String shortCode;

	@NotNull(message = "department can't be empty")
	private DepartmentBean department;

	public String getTenantCode() {
		return tenantCode;
	}

	public void setTenantCode(String tenantCode) {
		this.tenantCode = tenantCode;
	}

	public String getDesignationName() {
		return designationName;
	}

	public void setDesignationName(String designationName) {
		this.designationName = designationName;
	}

	public String getShortCode() {
		return shortCode;
	}

	public void setShortCode(String shortCode) {
		this.shortCode = shortCode;
	}

	public DepartmentBean getDepartment() {
		return department;
	}

	public void setDepartment(DepartmentBean department) {
		this.department = department;
	}

	@Override
	public String toString() {
		return "DesignationBean [tenantCode=" + tenantCode + ", designationName=" + designationName + ", shortCode="
				+ shortCode + ", department=" + department + ", getId()=" + getId() + ", getIsDeleted()="
				+ getIsDeleted() + "]";
	}

}
