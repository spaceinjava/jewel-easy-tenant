package com.spaceinje.tenant.bean;

import java.time.LocalDate;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;
import com.spaceinje.tenant.bean.base.BaseBean;

public class EmployeeBean extends BaseBean {
	
	@NotEmpty(message = "Tenant Code can't be empty")
	private String tenantCode;

	private Boolean generateId;

	private String employeeId;

	@NotEmpty(message = "First name can't be empty")
	private String firstName;

	private String middleName;

	private String lastName;

	private String gender;

	@Email
	private String email;

	private String mobile;

	private String photoUrl;

	private BranchBean branch;

	private DesignationBean designation;

	@JsonDeserialize(using = LocalDateDeserializer.class)
	@JsonSerialize(using = LocalDateSerializer.class)
	private LocalDate dob;

	private String emergencyContactNum;

	private String aadharNum;

	private String panNum;

	private AddressBean address;

	private Double salary;

	private BankDetailsBean bankDetails;

	private String passcode;

	private Boolean loginEnabled;
	
	

	public String getTenantCode() {
		return tenantCode;
	}

	public void setTenantCode(String tenantCode) {
		this.tenantCode = tenantCode;
	}

	public Boolean getGenerateId() {
		return generateId;
	}

	public void setGenerateId(Boolean generateId) {
		this.generateId = generateId;
	}

	public String getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(String employeeId) {
		this.employeeId = employeeId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getPhotoUrl() {
		return photoUrl;
	}

	public void setPhotoUrl(String photoUrl) {
		this.photoUrl = photoUrl;
	}

	public BranchBean getBranch() {
		return branch;
	}

	public void setBranch(BranchBean branch) {
		this.branch = branch;
	}

	public DesignationBean getDesignation() {
		return designation;
	}

	public void setDesignation(DesignationBean designation) {
		this.designation = designation;
	}

	public LocalDate getDob() {
		return dob;
	}

	public void setDob(LocalDate dob) {
		this.dob = dob;
	}


	public String getEmergencyContactNum() {
		return emergencyContactNum;
	}

	public void setEmergencyContactNum(String emergencyContactNum) {
		this.emergencyContactNum = emergencyContactNum;
	}

	public String getAadharNum() {
		return aadharNum;
	}

	public void setAadharNum(String aadharNum) {
		this.aadharNum = aadharNum;
	}

	public String getPanNum() {
		return panNum;
	}

	public void setPanNum(String panNum) {
		this.panNum = panNum;
	}

	public AddressBean getAddress() {
		return address;
	}

	public void setAddress(AddressBean address) {
		this.address = address;
	}

	public Double getSalary() {
		return salary;
	}

	public void setSalary(Double salary) {
		this.salary = salary;
	}

	public BankDetailsBean getBankDetails() {
		return bankDetails;
	}

	public void setBankDetails(BankDetailsBean bankDetails) {
		this.bankDetails = bankDetails;
	}

	public String getPasscode() {
		return passcode;
	}

	public void setPasscode(String passcode) {
		this.passcode = passcode;
	}

	public Boolean getLoginEnabled() {
		return loginEnabled;
	}

	public void setLoginEnabled(Boolean loginEnabled) {
		this.loginEnabled = loginEnabled;
	}

	@Override
	public String toString() {
		return "EmployeeBean [generateId=" + generateId + ", employeeId=" + employeeId + ", firstName=" + firstName
				+ ", middleName=" + middleName + ", lastName=" + lastName + ", gender=" + gender + ", email=" + email
				+ ", mobile=" + mobile + ", photoUrl=" + photoUrl + ", branch=" + branch + ", designation="
				+ designation + ", dob=" + dob + ", emergencyContactNum="
				+ emergencyContactNum + ", aadharNum=" + aadharNum + ", panNum=" + panNum + ", address=" + address
				+ ", salary=" + salary + ", bankDetails=" + bankDetails + ", passcode=" + passcode + ", loginEnabled="
				+ loginEnabled + "]";
	}

}
