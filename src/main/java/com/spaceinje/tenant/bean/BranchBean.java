package com.spaceinje.tenant.bean;

import java.util.Set;

import javax.validation.constraints.NotEmpty;

import com.spaceinje.tenant.bean.base.BaseBean;

public class BranchBean extends BaseBean {
	
	@NotEmpty(message = "Tenant Code can't be empty")
	private String tenantCode;

	private String branchName;

	private String shortCode;

	private String email;

	private String contactNum;

	private String alternateContactNum;

	private String gstNo;

	private AddressBean address;

	private Set<BankDetailsBean> bankDetails;

	private Boolean isHeadBranch;
	
	public String getTenantCode() {
		return tenantCode;
	}

	public void setTenantCode(String tenantCode) {
		this.tenantCode = tenantCode;
	}

	public String getBranchName() {
		return branchName;
	}

	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	public String getShortCode() {
		return shortCode;
	}

	public void setShortCode(String shortCode) {
		this.shortCode = shortCode;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getContactNum() {
		return contactNum;
	}

	public void setContactNum(String contactNum) {
		this.contactNum = contactNum;
	}

	public String getAlternateContactNum() {
		return alternateContactNum;
	}

	public void setAlternateContactNum(String alternateContactNum) {
		this.alternateContactNum = alternateContactNum;
	}

	public String getGstNo() {
		return gstNo;
	}

	public void setGstNo(String gstNo) {
		this.gstNo = gstNo;
	}

	public AddressBean getAddress() {
		return address;
	}

	public void setAddress(AddressBean address) {
		this.address = address;
	}

	public Set<BankDetailsBean> getBankDetails() {
		return bankDetails;
	}

	public void setBankDetails(Set<BankDetailsBean> bankDetails) {
		this.bankDetails = bankDetails;
	}

	public Boolean getIsHeadBranch() {
		return isHeadBranch;
	}

	public void setIsHeadBranch(Boolean isHeadBranch) {
		this.isHeadBranch = isHeadBranch;
	}

	@Override
	public String toString() {
		return "BranchBean [branchName=" + branchName + ", shortCode=" + shortCode + ", email=" + email
				+ ", contactNum=" + contactNum + ", alternateContactNum=" + alternateContactNum + ", gstNo=" + gstNo
				+ ", address=" + address + ", bankDetails=" + bankDetails + "]";
	}

}
