package com.spaceinje.tenant.bean;

import javax.validation.constraints.NotEmpty;

import com.spaceinje.tenant.bean.base.BaseBean;

public class BankDetailsBean extends BaseBean {

	@NotEmpty(message = "branch can't be empty")
	private String bankBranch;

	@NotEmpty(message = "bank name can't be empty")
	private String bankName;

	@NotEmpty(message = "account number can't be empty")
	private String accountNum;

	@NotEmpty(message = "account holder name can't be empty")
	private String accountHolderName;

	@NotEmpty(message = "ifsc code can't be empty")
	private String ifscCode;

	public String getBankBranch() {
		return bankBranch;
	}

	public void setBankBranch(String bankBranch) {
		this.bankBranch = bankBranch;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getAccountNum() {
		return accountNum;
	}

	public void setAccountNum(String accountNum) {
		this.accountNum = accountNum;
	}

	public String getAccountHolderName() {
		return accountHolderName;
	}

	public void setAccountHolderName(String accountHolderName) {
		this.accountHolderName = accountHolderName;
	}

	public String getIfscCode() {
		return ifscCode;
	}

	public void setIfscCode(String ifscCode) {
		this.ifscCode = ifscCode;
	}

	@Override
	public String toString() {
		return "BankDetailsBean [bankBranch=" + bankBranch + ", bankName=" + bankName + ", accountNum=" + accountNum
				+ ", accountHolderName=" + accountHolderName + ", ifscCode=" + ifscCode + "]";
	}

}
