package com.spaceinje.tenant.bean;

import javax.validation.constraints.NotNull;

import com.spaceinje.tenant.bean.base.BaseBean;

public class SupplierDetailsBean extends BaseBean {

	@NotNull(message = "Main group can't be null")
	private String mainGroupId;

	@NotNull(message = "Main group name can't be null")
	private String mainGroup;

	@NotNull(message = "Opening amount can't be null")
	private Double openingAmount;

	@NotNull(message = "Opening weight can't be null")
	private Double openingWeight;

	public String getMainGroupId() {
		return mainGroupId;
	}

	public void setMainGroupId(String mainGroupId) {
		this.mainGroupId = mainGroupId;
	}

	public String getMainGroup() {
		return mainGroup;
	}

	public void setMainGroup(String mainGroup) {
		this.mainGroup = mainGroup;
	}

	public Double getOpeningAmount() {
		return openingAmount;
	}

	public void setOpeningAmount(Double openingAmount) {
		this.openingAmount = openingAmount;
	}

	public Double getOpeningWeight() {
		return openingWeight;
	}

	public void setOpeningWeight(Double openingWeight) {
		this.openingWeight = openingWeight;
	}

	@Override
	public String toString() {
		return "SupplierDetailsBean [mainGroupId=" + mainGroupId + ", mainGroup=" + mainGroup + ", openingAmount="
				+ openingAmount + ", openingWeight=" + openingWeight + "]";
	}

}
