package com.spaceinje.tenant.bean;

import java.util.Set;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import com.spaceinje.tenant.bean.base.BaseBean;

public class SupplierBean extends BaseBean {

	@NotNull(message = "Party type can't be null")
	private String partyTypeId;

	@NotNull(message = "Party type name can't be null")
	private String partyType;

	@NotEmpty(message = "short code can't be null")
	private String shortCode;

	@NotEmpty(message = "Party Name can't be null")
	private String partyName;

	@NotEmpty(message = "Tenant code can't be null")
	private String tenantCode;

	@NotEmpty(message = "Email can't be null")
	@Email
	private String email;

	@NotEmpty(message = "Contact can't be null")
	private String contactNum;

	@NotNull(message = "Address can't be null")
	private AddressBean address;

	private String logoUrl;

	private String gstNum;

	private Double hallmarkChargesPerPc;

	private Boolean isSmith;

	private Boolean isBranded;

	private Boolean isGemStone;

	private Boolean isTestingDealer;

	@NotNull(message = "Bank Details can't be null")
	private Set<BankDetailsBean> bankDetails;

	private Set<SupplierDetailsBean> supplierDetails;

	public String getPartyTypeId() {
		return partyTypeId;
	}

	public void setPartyTypeId(String partyTypeId) {
		this.partyTypeId = partyTypeId;
	}

	public String getPartyType() {
		return partyType;
	}

	public void setPartyType(String partyType) {
		this.partyType = partyType;
	}

	public String getShortCode() {
		return shortCode;
	}

	public void setShortCode(String shortCode) {
		this.shortCode = shortCode;
	}

	public String getPartyName() {
		return partyName;
	}

	public void setPartyName(String partyName) {
		this.partyName = partyName;
	}

	public String getTenantCode() {
		return tenantCode;
	}

	public void setTenantCode(String tenantCode) {
		this.tenantCode = tenantCode;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getContactNum() {
		return contactNum;
	}

	public void setContactNum(String contactNum) {
		this.contactNum = contactNum;
	}

	public AddressBean getAddress() {
		return address;
	}

	public void setAddress(AddressBean address) {
		this.address = address;
	}

	public String getLogoUrl() {
		return logoUrl;
	}

	public void setLogoUrl(String logoUrl) {
		this.logoUrl = logoUrl;
	}

	public String getGstNum() {
		return gstNum;
	}

	public void setGstNum(String gstNum) {
		this.gstNum = gstNum;
	}

	public Double getHallmarkChargesPerPc() {
		return hallmarkChargesPerPc;
	}

	public void setHallmarkChargesPerPc(Double hallmarkChargesPerPc) {
		this.hallmarkChargesPerPc = hallmarkChargesPerPc;
	}

	public Boolean getIsSmith() {
		return isSmith;
	}

	public void setIsSmith(Boolean isSmith) {
		this.isSmith = isSmith;
	}

	public Boolean getIsBranded() {
		return isBranded;
	}

	public void setIsBranded(Boolean isBranded) {
		this.isBranded = isBranded;
	}

	public Boolean getIsGemStone() {
		return isGemStone;
	}

	public void setIsGemStone(Boolean isGemStone) {
		this.isGemStone = isGemStone;
	}

	public Boolean getIsTestingDealer() {
		return isTestingDealer;
	}

	public void setIsTestingDealer(Boolean isTestingDealer) {
		this.isTestingDealer = isTestingDealer;
	}

	public Set<BankDetailsBean> getBankDetails() {
		return bankDetails;
	}

	public void setBankDetails(Set<BankDetailsBean> bankDetails) {
		this.bankDetails = bankDetails;
	}

	public Set<SupplierDetailsBean> getSupplierDetails() {
		return supplierDetails;
	}

	public void setSupplierDetails(Set<SupplierDetailsBean> supplierDetails) {
		this.supplierDetails = supplierDetails;
	}

	@Override
	public String toString() {
		return "SupplierBean [partyTypeId=" + partyTypeId + ", partyType=" + partyType + ", shortCode=" + shortCode
				+ ", partyName=" + partyName + ", tenantCode=" + tenantCode + ", email=" + email + ", contactNum="
				+ contactNum + ", address=" + address + ", logoUrl=" + logoUrl + ", gstNum=" + gstNum
				+ ", hallmarkChargesPerPc=" + hallmarkChargesPerPc + ", isSmith=" + isSmith + ", isBranded=" + isBranded
				+ ", isGemStone=" + isGemStone + ", isTestingDealer=" + isTestingDealer + ", bankDetails=" + bankDetails
				+ ", supplierDetails=" + supplierDetails + "]";
	}

}
