package com.spaceinje.tenant.bean;

public class UserInfoBean {

	private String id;

	private Boolean isTenant;
	
	private Boolean isDeleted;

//	private String admin_code;

	private String tenantCode;

//	private String emp_code;

	private String firstName;

	private String middleName;

	private String lastName;

	private String email;

	private String mobile;

	private String userName;

	private String password;

//	private String profile_picture;

	private Integer loginAttempts;

	private String loginStatus;

	private String hash;

	private String secret;


	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Boolean getIsTenant() {
		return isTenant;
	}

	public void setIsTenant(Boolean isTenant) {
		this.isTenant = isTenant;
	}

	public Boolean getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public String getTenantCode() {
		return tenantCode;
	}

	public void setTenantCode(String tenantCode) {
		this.tenantCode = tenantCode;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Integer getLoginAttempts() {
		return loginAttempts;
	}

	public void setLoginAttempts(Integer loginAttempts) {
		this.loginAttempts = loginAttempts;
	}

	public String getLoginStatus() {
		return loginStatus;
	}

	public void setLoginStatus(String loginStatus) {
		this.loginStatus = loginStatus;
	}

	public String getHash() {
		return hash;
	}

	public void setHash(String hash) {
		this.hash = hash;
	}

	public String getSecret() {
		return secret;
	}

	public void setSecret(String secret) {
		this.secret = secret;
	}

	@Override
	public String toString() {
		return "UserInfoBean [id=" + id + ", isTenant=" + isTenant + ", isDeleted=" + isDeleted + ", tenantCode="
				+ tenantCode + ", firstName=" + firstName + ", middleName=" + middleName + ", lastName=" + lastName
				+ ", email=" + email + ", mobile=" + mobile + ", userName=" + userName + ", password=" + password
				+ ", loginAttempts=" + loginAttempts + ", loginStatus=" + loginStatus + ", hash=" + hash + ", secret="
				+ secret + "]";
	}




}
