package com.spaceinje.tenant.bean;

import javax.validation.constraints.NotEmpty;

import com.spaceinje.tenant.bean.base.BaseBean;

public class DepartmentBean extends BaseBean {

	@NotEmpty(message = "tenant code can't be empty")
	private String tenantCode;

	@NotEmpty(message = "department name can't be empty")
	private String departmentName;

	@NotEmpty(message = "short code can't be empty")
	private String shortCode;

	public String getTenantCode() {
		return tenantCode;
	}

	public void setTenantCode(String tenantCode) {
		this.tenantCode = tenantCode;
	}

	public String getDepartmentName() {
		return departmentName;
	}

	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}

	public String getShortCode() {
		return shortCode;
	}

	public void setShortCode(String shortCode) {
		this.shortCode = shortCode;
	}

	@Override
	public String toString() {
		return "DepartmentBean [tenantCode=" + tenantCode + ", departmentName=" + departmentName + ", shortCode="
				+ shortCode + "]";
	}

}
