package com.spaceinje.tenant.bean;

import javax.validation.constraints.NotEmpty;

import com.spaceinje.tenant.bean.base.BaseBean;

public class AddressBean extends BaseBean {

	@NotEmpty(message = "address line 1 can't be null")
	private String addressLine1;

	private String addressLine2;

	@NotEmpty(message = "area can't be null")
	private String area;

	@NotEmpty(message = "city can't be null")
	private String city;

	@NotEmpty(message = "district can't be null")
	private String district;

	@NotEmpty(message = "state can't be null")
	private String state;

	@NotEmpty(message = "pincode can't be null")
	private String pincode;

	public String getAddressLine1() {
		return addressLine1;
	}

	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}

	public String getAddressLine2() {
		return addressLine2;
	}

	public void setAddressLine2(String addressLine2) {
		this.addressLine2 = addressLine2;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getDistrict() {
		return district;
	}

	public void setDistrict(String district) {
		this.district = district;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getPincode() {
		return pincode;
	}

	public void setPincode(String pincode) {
		this.pincode = pincode;
	}

	@Override
	public String toString() {
		return "AddressBean [addressLine1=" + addressLine1 + ", addressLine2=" + addressLine2 + ", area=" + area
				+ ", city=" + city + ", district=" + district + ", state=" + state + ", pincode=" + pincode + "]";
	}

}
