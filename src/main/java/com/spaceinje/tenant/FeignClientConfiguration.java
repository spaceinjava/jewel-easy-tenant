package com.spaceinje.tenant;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import feign.auth.BasicAuthRequestInterceptor;

@Configuration
public class FeignClientConfiguration {

	@Value("${spring.cloud.config.usernameAuth}")
	private String userName;

	@Value("${spring.cloud.config.passwordAuth}")
	private String passWord;

	@Bean
	public BasicAuthRequestInterceptor basicAuthRequestInterceptor() {
		return new BasicAuthRequestInterceptor(userName, passWord);
	}
}
