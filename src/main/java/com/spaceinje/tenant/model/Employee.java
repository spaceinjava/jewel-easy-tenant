package com.spaceinje.tenant.model;

import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import org.hibernate.annotations.DynamicUpdate;

import com.spaceinje.tenant.model.base.AuditEntity;

@Entity
@DynamicUpdate
public class Employee extends AuditEntity implements Serializable {

	private static final long serialVersionUID = 8177907214680789012L;

	private Boolean generateId;

	private String employeeId;

	private String firstName;

	private String middleName;

	private String lastName;

	private String gender;

	private String email;

	private String mobile;

	private String photoUrl;

	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "branchId", nullable = false)
	private Branch branch;

	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "designationId", nullable = false)
	private Designation designation;

	private LocalDate dob;

	private String emergencyContactNum;

	private String aadharNum;

	private String panNum;

	@OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "employee")
	private Address address;

	private Double salary;

	@OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "employee")
	private BankDetails bankDetails;

	private String passcode;

	private Boolean loginEnabled;

	public Boolean getGenerateId() {
		return generateId;
	}

	public void setGenerateId(Boolean generateId) {
		this.generateId = generateId;
	}

	public String getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(String employeeId) {
		this.employeeId = employeeId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getPhotoUrl() {
		return photoUrl;
	}

	public void setPhotoUrl(String photoUrl) {
		this.photoUrl = photoUrl;
	}

	public Branch getBranch() {
		return branch;
	}

	public void setBranch(Branch branch) {
		this.branch = branch;
	}

	public Designation getDesignation() {
		return designation;
	}

	public void setDesignation(Designation designation) {
		this.designation = designation;
	}

	public LocalDate getDob() {
		return dob;
	}

	public void setDob(LocalDate dob) {
		this.dob = dob;
	}

	public String getEmergencyContactNum() {
		return emergencyContactNum;
	}

	public void setEmergencyContactNum(String emergencyContactNum) {
		this.emergencyContactNum = emergencyContactNum;
	}

	public String getAadharNum() {
		return aadharNum;
	}

	public void setAadharNum(String aadharNum) {
		this.aadharNum = aadharNum;
	}

	public String getPanNum() {
		return panNum;
	}

	public void setPanNum(String panNum) {
		this.panNum = panNum;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public Double getSalary() {
		return salary;
	}

	public void setSalary(Double salary) {
		this.salary = salary;
	}

	public BankDetails getBankDetails() {
		return bankDetails;
	}

	public void setBankDetails(BankDetails bankDetails) {
		this.bankDetails = bankDetails;
	}

	public String getPasscode() {
		return passcode;
	}

	public void setPasscode(String passcode) {
		this.passcode = passcode;
	}

	public Boolean getLoginEnabled() {
		return loginEnabled;
	}

	public void setLoginEnabled(Boolean loginEnabled) {
		this.loginEnabled = loginEnabled;
	}

	@Override
	public String toString() {
		return "Employee [generateId=" + generateId + ", employeeId=" + employeeId + ", firstName=" + firstName
				+ ", middleName=" + middleName + ", lastName=" + lastName + ", gender=" + gender + ", email=" + email
				+ ", mobile=" + mobile + ", photoUrl=" + photoUrl + ", branch=" + branch + ", designation="
				+ designation + ", dob=" + dob + ", emergencyContactNum=" + emergencyContactNum + ", aadharNum="
				+ aadharNum + ", panNum=" + panNum + ", address=" + address + ", salary=" + salary + ", bankDetails="
				+ bankDetails + ", passcode=" + passcode + ", loginEnabled=" + loginEnabled + "]";
	}

}
