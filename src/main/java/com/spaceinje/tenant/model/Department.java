package com.spaceinje.tenant.model;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;

import com.spaceinje.tenant.model.base.AuditEntity;

@Entity
public class Department extends AuditEntity implements Serializable {

	private static final long serialVersionUID = 1727586506494356566L;

	private String departmentName;

	private String shortCode;

	@OneToMany(mappedBy = "department", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private Set<Designation> designations;

	public String getDepartmentName() {
		return departmentName;
	}

	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}

	public String getShortCode() {
		return shortCode;
	}

	public void setShortCode(String shortCode) {
		this.shortCode = shortCode;
	}

	public Set<Designation> getDesignations() {
		return designations;
	}

	public void setDesignations(Set<Designation> designations) {
		this.designations = designations;
	}

	@Override
	public String toString() {
		return "Department [departmentName=" + departmentName + ", shortCode=" + shortCode + ", designations="
				+ designations + ", getId()=" + getId() + ", getIsDeleted()=" + getIsDeleted() + ", getTenantCode()="
				+ getTenantCode() + "]";
	}

}
