package com.spaceinje.tenant.model;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import com.spaceinje.tenant.model.base.AuditEntity;

@Entity
public class Supplier extends AuditEntity implements Serializable {

	private static final long serialVersionUID = -8762316540039984588L;

	private String partyTypeId;

	private String partyType;

	private String shortCode;

	private String partyName;

	private String tenantCode;

	private String email;

	private String contactNum;

	@OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "supplier")
	private Address address;

	private String logoUrl;

	private String gstNum;

	private Double hallmarkChargesPerPc;

	private Boolean isSmith;

	private Boolean isBranded;

	private Boolean isGemStone;

	private Boolean isTestingDealer;

	@OneToMany(mappedBy = "supplier", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private Set<BankDetails> bankDetails;

	@OneToMany(mappedBy = "supplier", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private Set<SupplierDetails> supplierDetails;

	public String getPartyTypeId() {
		return partyTypeId;
	}

	public void setPartyTypeId(String partyTypeId) {
		this.partyTypeId = partyTypeId;
	}

	public String getPartyType() {
		return partyType;
	}

	public void setPartyType(String partyType) {
		this.partyType = partyType;
	}

	public String getShortCode() {
		return shortCode;
	}

	public void setShortCode(String shortCode) {
		this.shortCode = shortCode;
	}

	public String getPartyName() {
		return partyName;
	}

	public void setPartyName(String partyName) {
		this.partyName = partyName;
	}

	public String getTenantCode() {
		return tenantCode;
	}

	public void setTenantCode(String tenantCode) {
		this.tenantCode = tenantCode;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getContactNum() {
		return contactNum;
	}

	public void setContactNum(String contactNum) {
		this.contactNum = contactNum;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public String getLogoUrl() {
		return logoUrl;
	}

	public void setLogoUrl(String logoUrl) {
		this.logoUrl = logoUrl;
	}

	public String getGstNum() {
		return gstNum;
	}

	public void setGstNum(String gstNum) {
		this.gstNum = gstNum;
	}

	public Double getHallmarkChargesPerPc() {
		return hallmarkChargesPerPc;
	}

	public void setHallmarkChargesPerPc(Double hallmarkChargesPerPc) {
		this.hallmarkChargesPerPc = hallmarkChargesPerPc;
	}

	public Boolean getIsSmith() {
		return isSmith;
	}

	public void setIsSmith(Boolean isSmith) {
		this.isSmith = isSmith;
	}

	public Boolean getIsBranded() {
		return isBranded;
	}

	public void setIsBranded(Boolean isBranded) {
		this.isBranded = isBranded;
	}

	public Boolean getIsGemStone() {
		return isGemStone;
	}

	public void setIsGemStone(Boolean isGemStone) {
		this.isGemStone = isGemStone;
	}

	public Boolean getIsTestingDealer() {
		return isTestingDealer;
	}

	public void setIsTestingDealer(Boolean isTestingDealer) {
		this.isTestingDealer = isTestingDealer;
	}

	public Set<BankDetails> getBankDetails() {
		return bankDetails;
	}

	public void setBankDetails(Set<BankDetails> bankDetails) {
		this.bankDetails = bankDetails;
	}

	public Set<SupplierDetails> getSupplierDetails() {
		return supplierDetails;
	}

	public void setSupplierDetails(Set<SupplierDetails> supplierDetails) {
		this.supplierDetails = supplierDetails;
	}

	@Override
	public String toString() {
		return "Supplier [partyTypeId=" + partyTypeId + ", partyType=" + partyType + ", shortCode=" + shortCode
				+ ", partyName=" + partyName + ", tenantCode=" + tenantCode + ", email=" + email + ", contactNum="
				+ contactNum + ", address=" + address + ", logoUrl=" + logoUrl + ", gstNum=" + gstNum
				+ ", hallmarkChargesPerPc=" + hallmarkChargesPerPc + ", isSmith=" + isSmith + ", isBranded=" + isBranded
				+ ", isGemStone=" + isGemStone + ", isTestingDealer=" + isTestingDealer + ", bankDetails=" + bankDetails
				+ ", supplierDetails=" + supplierDetails + "]";
	}

}
