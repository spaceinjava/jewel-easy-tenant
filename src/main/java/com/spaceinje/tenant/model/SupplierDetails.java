package com.spaceinje.tenant.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.spaceinje.tenant.model.base.AuditEntity;

@Entity
public class SupplierDetails extends AuditEntity implements Serializable {

	private static final long serialVersionUID = -2405045822369751511L;

	private String mainGroupId;

	private String mainGroup;

	private Double openingAmount;

	private Double openingWeight;

	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "supplierId", nullable = false)
	private Supplier supplier;

	public String getMainGroupId() {
		return mainGroupId;
	}

	public void setMainGroupId(String mainGroupId) {
		this.mainGroupId = mainGroupId;
	}

	public String getMainGroup() {
		return mainGroup;
	}

	public void setMainGroup(String mainGroup) {
		this.mainGroup = mainGroup;
	}

	public Double getOpeningAmount() {
		return openingAmount;
	}

	public void setOpeningAmount(Double openingAmount) {
		this.openingAmount = openingAmount;
	}

	public Double getOpeningWeight() {
		return openingWeight;
	}

	public void setOpeningWeight(Double openingWeight) {
		this.openingWeight = openingWeight;
	}

	public Supplier getSupplier() {
		return supplier;
	}

	public void setSupplier(Supplier supplier) {
		this.supplier = supplier;
	}

	@Override
	public String toString() {
		return "SupplierDetails [mainGroupId=" + mainGroupId + ", mainGroup=" + mainGroup + ", openingAmount="
				+ openingAmount + ", openingWeight=" + openingWeight + ", supplier=" + supplier + "]";
	}

}
