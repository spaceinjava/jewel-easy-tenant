/**
 * 
 */
package com.spaceinje.tenant.model.base;

import java.util.Optional;

import org.springframework.data.domain.AuditorAware;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

/**
 * @author Lakshmi Kiran
 * @implNote class which audits the user information on entities
 * @version 1.0
 */
@Component
public class UserAuditing implements AuditorAware<String>{

	@Override
	public Optional<String> getCurrentAuditor() {
		String uname = SecurityContextHolder.getContext().getAuthentication().getName();
        return Optional.of(uname);
	}

}
