package com.spaceinje.tenant.model.base;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@Entity(name = "bank_ifsc")
@JsonInclude(Include.NON_NULL)
public class Bank implements Serializable {

	private static final long serialVersionUID = -2618528595498893984L;

	private String branch;

	private String bank;
	
	@Id
	private String ifsc;

	public String getBranch() {
		return branch;
	}

	public void setBranch(String branch) {
		this.branch = branch;
	}

	public String getBank() {
		return bank;
	}

	public void setBank(String bank) {
		this.bank = bank;
	}

	public String getIfsc() {
		return ifsc;
	}

	public void setIfsc(String ifsc) {
		this.ifsc = ifsc;
	}

	@Override
	public String toString() {
		return "Bank [branch=" + branch + ", bank=" + bank + ", ifsc=" + ifsc + "]";
	}

	
}
