package com.spaceinje.tenant.model;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import com.spaceinje.tenant.model.base.AuditEntity;

@Entity
public class Branch extends AuditEntity implements Serializable {

	private static final long serialVersionUID = -453142801348028767L;

	private String branchName;

	private String shortCode;

	private String email;

	private String contactNum;

	private String alternateContactNum;

	private String gstNo;

	private boolean isHeadBranch;

	@OneToOne(mappedBy = "branch", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private Address address;

	@OneToMany(mappedBy = "branch", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private Set<BankDetails> bankDetails;

	public String getBranchName() {
		return branchName;
	}

	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	public String getShortCode() {
		return shortCode;
	}

	public void setShortCode(String shortCode) {
		this.shortCode = shortCode;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getContactNum() {
		return contactNum;
	}

	public void setContactNum(String contactNum) {
		this.contactNum = contactNum;
	}

	public String getAlternateContactNum() {
		return alternateContactNum;
	}

	public void setAlternateContactNum(String alternateContactNum) {
		this.alternateContactNum = alternateContactNum;
	}

	public String getGstNo() {
		return gstNo;
	}

	public void setGstNo(String gstNo) {
		this.gstNo = gstNo;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public Set<BankDetails> getBankDetails() {
		return bankDetails;
	}

	public void setBankDetails(Set<BankDetails> bankDetails) {
		this.bankDetails = bankDetails;
	}

	public boolean getIsHeadBranch() {
		return isHeadBranch;
	}

	public void setIsHeadBranch(boolean isHeadBranch) {
		this.isHeadBranch = isHeadBranch;
	}

	@Override
	public String toString() {
		return "Branch [branchName=" + branchName + ", shortCode=" + shortCode + ", email=" + email + ", contactNum="
				+ contactNum + ", alternateContactNum=" + alternateContactNum + ", gstNo=" + gstNo + ", isHeadBranch="
				+ isHeadBranch + ", address=" + address + ", bankDetails=" + bankDetails + "]";
	}

}
