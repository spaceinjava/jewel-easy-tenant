package com.spaceinje.tenant.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.spaceinje.tenant.model.base.AuditEntity;

@Entity
public class Designation extends AuditEntity implements Serializable {

	private static final long serialVersionUID = 672329290227953589L;

	private String designationName;

	private String shortCode;

	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "departmentId", nullable = false)
	private Department department;

	public String getDesignationName() {
		return designationName;
	}

	public void setDesignationName(String designationName) {
		this.designationName = designationName;
	}

	public String getShortCode() {
		return shortCode;
	}

	public void setShortCode(String shortCode) {
		this.shortCode = shortCode;
	}

	public Department getDepartment() {
		return department;
	}

	public void setDepartment(Department department) {
		this.department = department;
	}

	@Override
	public String toString() {
		return "Designation [designationName=" + designationName + ", shortCode=" + shortCode + ", department="
				+ department + "]";
	}

}
