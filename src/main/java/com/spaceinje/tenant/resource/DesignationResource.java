package com.spaceinje.tenant.resource;

import java.util.Set;

import javax.validation.Valid;

import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.spaceinje.tenant.bean.DesignationBean;
import com.spaceinje.tenant.constants.TenantConstants;
import com.spaceinje.tenant.service.DesignationService;
import com.spaceinje.tenant.util.ResponseCachet;
import com.spaceinje.tenant.validator.TenantValidator;

import brave.Tracer;

@SuppressWarnings("rawtypes")
@RestController
@RequestMapping("/v1/tenant/designation")
public class DesignationResource {

	private static final Logger log = LoggerFactory.getLogger(DesignationResource.class);

	@Autowired
	private DesignationService designationService;

	@Autowired
	private TenantValidator validator;

	@Autowired
	private TenantConstants constants;

	@Autowired
	private Tracer tracer;

	@SuppressWarnings("unchecked")
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ExceptionHandler(MethodArgumentNotValidException.class)
	public JSONObject handleValidationExceptions(MethodArgumentNotValidException ex) {
		JSONObject errors = new JSONObject();
		ex.getBindingResult().getAllErrors().forEach((error) -> {
			String fieldName = ((FieldError) error).getField();
			String errorMessage = error.getDefaultMessage();
			errors.put(fieldName, errorMessage);
		});
		return errors;
	}
	
	@PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResponseCachet> saveOrUpdateDesignation(@Valid @RequestBody DesignationBean bean)
			throws MethodArgumentNotValidException {
		ResponseEntity<ResponseCachet> entity = validator.designationValidator(bean);
		if (entity.getStatusCode() != HttpStatus.OK) {
			log.debug("error while validating designation bean");
			return entity;
		}
		log.debug("calling saveOrUpdateDesignation service");
		ResponseCachet cachet = designationService.saveOrUpdateDesignation(bean);

		if (cachet.getStatus().equals(constants.getStatus_message_failure())) {
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_IMPLEMENTED);
		} else if (cachet.getStatus().equals(constants.getStatus_message_exception())) {
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		log.debug("Exiting saveOrUpdateDesignation service");
		return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.OK);
	}

	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResponseCachet> fetchAllDesignations(@RequestParam(value = "tenant_code") String tenantCode) {
		ResponseCachet cachet = new ResponseCachet();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		if (StringUtils.isEmpty(tenantCode)) {
			log.debug("Tenant code can't be empty");
			cachet.setStatus(constants.getStatus_message_failure());
			cachet.setMessage("Tenant code can't be empty");
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_ACCEPTABLE);
		}
		log.debug("calling fetchAllDesignations service");
		cachet = designationService.fetchAllDesignations(tenantCode);
		if (cachet.getStatus().equals(constants.getStatus_message_failure())) {
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_IMPLEMENTED);
		} else if (cachet.getStatus().equals(constants.getStatus_message_exception())) {
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		log.debug("Exiting fetchAllDesignations service");
		return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.OK);
	}

	@DeleteMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResponseCachet> deleteDesignations(
			@RequestParam(value = "designation_ids") Set<String> designationIds) {
		ResponseCachet cachet = new ResponseCachet();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		if (CollectionUtils.isEmpty(designationIds)) {
			log.debug("designationIds can't be empty");
			cachet.setStatus(constants.getStatus_message_failure());
			cachet.setMessage("designationIds can't be empty");
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_ACCEPTABLE);
		}
		log.debug("calling deleteDesignations service");
		cachet = designationService.deleteDesignations(designationIds);
		if (cachet.getStatus().equals(constants.getStatus_message_failure())) {
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_IMPLEMENTED);
		} else if (cachet.getStatus().equals(constants.getStatus_message_exception())) {
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		log.debug("Exiting deleteDesignations service");
		return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.OK);
	}

}
