package com.spaceinje.tenant.resource;

import java.util.List;
import java.util.Set;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.spaceinje.tenant.bean.BranchBean;
import com.spaceinje.tenant.constants.TenantConstants;
import com.spaceinje.tenant.service.BranchService;
import com.spaceinje.tenant.util.ResponseCachet;
import com.spaceinje.tenant.validator.BranchValidator;

import brave.Tracer;

@RestController
@RequestMapping("/v1/tenant/branch")
public class BranchResource {

	private static final Logger log = LoggerFactory.getLogger(BranchResource.class);

	@Autowired
	private BranchService branchService;

	@Autowired
	private TenantConstants constants;

	@Autowired
	private Tracer tracer;

	@Autowired
	private BranchValidator branchValidator;

	/**
	 * service to save branch
	 * 
	 * @param bean - UserInfoBean
	 * 
	 * @return - {@value ResponseEntity<ResponseCachet>}
	 * 
	 */
	@PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResponseCachet<BranchBean>> saveBranch(@Valid @RequestBody BranchBean bean) {
		log.debug("Entering Save method");

		ResponseCachet<BranchBean> cachet = new ResponseCachet<>();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));

		ResponseCachet<List<String>> valCachet = branchValidator.validateBranch(bean);

		if (valCachet.getData().size() > 0) {
			cachet.setStatus(constants.getStatus_message_failure());
			cachet.setMessage(String.join(",", valCachet.getData()));
			return new ResponseEntity<ResponseCachet<BranchBean>>(cachet, HttpStatus.BAD_REQUEST);
		}
		log.info("calling branch service");
		cachet = branchService.saveBranch(bean);
		if (cachet.getStatus().equals(constants.getStatus_message_exception())) {
			log.debug("Exiting saveBranch service");
			return new ResponseEntity<ResponseCachet<BranchBean>>(cachet, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		log.debug("Exiting saveBranch service");
		return new ResponseEntity<ResponseCachet<BranchBean>>(cachet, HttpStatus.OK);

	}

	/**
	 * service to list all branches of tenant
	 * 
	 * @param tenant_code
	 * 
	 * @return - {@value ResponseEntity<ResponseCachet>}
	 */
	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResponseCachet<List<BranchBean>>> findAllBranchs(
			@RequestParam("tenant_code") String tenantCode) {
		log.info("Entering listAllBranchs method");

		ResponseCachet<List<BranchBean>> cachet = new ResponseCachet<>();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));

		cachet = branchService.fetchAllBranches(tenantCode);
		if (cachet.getStatus().equals(constants.getStatus_message_exception())) {
			log.debug("Exiting listAllBranchs service");
			return new ResponseEntity<ResponseCachet<List<BranchBean>>>(cachet, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		log.info("Exiting listAllBranches service");
		return new ResponseEntity<ResponseCachet<List<BranchBean>>>(cachet, HttpStatus.OK);

	}

	/**
	 * service to delete branches
	 * 
	 * @param branch_ids - id's of branches
	 * 
	 * @return - {@value ResponseEntity<ResponseCachet>}
	 */
	@SuppressWarnings({ "rawtypes" })
	@RequestMapping(method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResponseCachet> deleteBranchs(@RequestParam("branch_ids") Set<String> branch_ids){
		log.debug("Entering deleteBranch method");

		ResponseCachet cachet = new ResponseCachet();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		if (CollectionUtils.isEmpty(branch_ids)) {
			log.error("Branch Ids can't be empty");
			cachet.setStatus(constants.getStatus_message_failure());
			cachet.setMessage("Branch Ids can't be empty");
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_ACCEPTABLE);
		}
		log.info("calling  Branchservice");
		cachet = branchService.deleteBranches(branch_ids);
		if (cachet.getStatus().equals(constants.getStatus_message_failure())) {
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_IMPLEMENTED);
		} else if (cachet.getStatus().equals(constants.getStatus_message_exception())) {
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		log.info("Exiting Branch Ids service");
		return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.OK);

	}

}
