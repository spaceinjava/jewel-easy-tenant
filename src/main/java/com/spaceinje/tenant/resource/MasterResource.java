/**
 * 
 */
package com.spaceinje.tenant.resource;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.spaceinje.tenant.bean.base.BankBean;
import com.spaceinje.tenant.bean.base.PostalBean;
import com.spaceinje.tenant.constants.TenantConstants;
import com.spaceinje.tenant.service.MasterDataService;
import com.spaceinje.tenant.util.ResponseCachet;

import brave.Tracer;

@SuppressWarnings("rawtypes")
@RestController
@RequestMapping("/v1/tenant")
public class MasterResource {

	private static final Logger log = LoggerFactory.getLogger(MasterResource.class);

	@Autowired
	private MasterDataService masterDataService;

	@Autowired
	private TenantConstants constants;

	@Autowired
	private Tracer tracer;

	/**
	 * resource which retrieves address information using pincode
	 * 
	 * @param pincode
	 * 
	 * @return @ResponseEntity<ResponseCachet<List<PostalBean>>>
	 */
	@GetMapping(value = "/postal/{pincode}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResponseCachet> findPostal(@PathVariable("pincode") String pincode) {
		if (log.isDebugEnabled())
			log.debug("Entering findPostal service");

		ResponseCachet<List<PostalBean>> cachet = new ResponseCachet<List<PostalBean>>();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		if (StringUtils.isEmpty(pincode)) {
			log.info("pincode can't be empty");
			cachet.setStatus(constants.getStatus_message_failure());
			cachet.setMessage("pincode can't be empty");
			if (log.isDebugEnabled())
				log.debug("Exiting findPostal service");
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_ACCEPTABLE);
		}
		log.info("calling postal service");
		cachet = masterDataService.getPostalDetailsByPincode(pincode);
		if (cachet.getStatus().equals(constants.getStatus_message_failure())) {
			if (log.isDebugEnabled())
				log.debug("Exiting findPostal service");
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_IMPLEMENTED);
		}
		if (cachet.getStatus().equals(constants.getStatus_message_exception())) {
			if (log.isDebugEnabled())
				log.debug("Exiting findPostal service");
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		if (log.isDebugEnabled())
			log.debug("Exiting findPostal service");
		return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.OK);
	}

	/**
	 * resource which retrieves bank information using IFSC
	 * 
	 * @param ifsc
	 * 
	 * @return @ResponseEntity<ResponseCachet<BankBranchBean>>
	 */
	@GetMapping(value = "/ifsc/{ifsc}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResponseCachet> findIFSC(@PathVariable("ifsc") String ifsc) {
		if (log.isDebugEnabled())
			log.debug("Entering findIFSC service");

		ResponseCachet<BankBean> cachet = new ResponseCachet<BankBean>();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		if (StringUtils.isEmpty(ifsc)) {
			log.info("ifsc can't be empty");
			cachet.setStatus(constants.getStatus_message_failure());
			cachet.setMessage("ifsc can't be empty");
			if (log.isDebugEnabled())
				log.debug("Exiting findIFSC service");
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_ACCEPTABLE);
		}
		log.info("calling bank details service");
		cachet = masterDataService.getBankDetailsByIfsc(ifsc);
		if (cachet.getStatus().equals(constants.getStatus_message_failure())) {
			if (log.isDebugEnabled())
				log.debug("Exiting findIFSC service");
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_IMPLEMENTED);
		}
		if (cachet.getStatus().equals(constants.getStatus_message_exception())) {
			if (log.isDebugEnabled())
				log.debug("Exiting findIFSC service");
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		if (log.isDebugEnabled())
			log.debug("Exiting findIFSC service");
		return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.OK);
	}

}
