package com.spaceinje.tenant.resource;

import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.spaceinje.tenant.bean.EmployeeBean;
import com.spaceinje.tenant.constants.TenantConstants;
import com.spaceinje.tenant.service.EmployeeService;
import com.spaceinje.tenant.util.ResponseCachet;
import com.spaceinje.tenant.validator.EmployeeValidator;

import brave.Tracer;

@RestController
@RequestMapping("/v1/tenant/employee")
public class EmployeeResource {

	private static final Logger log = LoggerFactory.getLogger(EmployeeResource.class);

	@Autowired
	private EmployeeService employeeService;

	@Autowired
	private TenantConstants constants;

	@Autowired
	private Tracer tracer;

	@Autowired
	private EmployeeValidator employeeValidator;

	/**
	 * service to save employee
	 * 
	 * @param bean - UserInfoBean
	 * 
	 * @return - {@value ResponseEntity<ResponseCachet>}
	 * @throws JsonProcessingException
	 * @throws JsonMappingException
	 * 
	 */
	@PostMapping(produces = MediaType.APPLICATION_JSON_VALUE, consumes = { MediaType.MULTIPART_FORM_DATA_VALUE })
	public ResponseEntity<ResponseCachet<EmployeeBean>> saveEmployee(
			@RequestPart(value = "image", required = false) MultipartFile file, @RequestPart("bean") String bean)
			throws JsonMappingException, JsonProcessingException {
		log.debug("Entering saveEmployee method");

		ResponseCachet<EmployeeBean> cachet = new ResponseCachet<>();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));

		EmployeeBean employeeBean = new ObjectMapper().readValue(bean, EmployeeBean.class);
		ResponseCachet<List<String>> valCachet = employeeValidator.validateEmployee(employeeBean);

		if (valCachet.getData().size() > 0) {

			cachet.setStatus(constants.getStatus_message_failure());
			cachet.setMessage(String.join(",", valCachet.getData()));
			return new ResponseEntity<ResponseCachet<EmployeeBean>>(cachet, HttpStatus.BAD_REQUEST);
		}

		log.info("calling employee service");
		cachet = employeeService.saveOrUpdateEmployee(employeeBean, file);
		if (cachet.getStatus().equals(constants.getStatus_message_failure())) {
			log.debug("Exiting saveEmployee service");
			return new ResponseEntity<ResponseCachet<EmployeeBean>>(cachet, HttpStatus.NOT_IMPLEMENTED);
		}
		if (cachet.getStatus().equals(constants.getStatus_message_exception())) {
			log.debug("Exiting saveEmployee service");
			return new ResponseEntity<ResponseCachet<EmployeeBean>>(cachet, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		log.debug("Exiting saveEmployee service");
		return new ResponseEntity<ResponseCachet<EmployeeBean>>(cachet, HttpStatus.OK);

	}

	/**
	 * service to list all employees of tenant
	 * 
	 * @param tenant_code - email of tenant
	 * 
	 * @return - {@value ResponseEntity<ResponseCachet>}
	 */
	@SuppressWarnings({ "rawtypes" })
	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResponseCachet> findAllEmployees(@RequestParam("tenant_code") String tenant_code) {
		log.info("Entering listAllEmployees method");

		ResponseCachet cachet = new ResponseCachet<>();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));

		log.info("validating the request");
		if (StringUtils.isEmpty(tenant_code)) {
			log.info("request can't be empty");

			cachet.setStatus(constants.getStatus_message_failure());
			cachet.setMessage("request can't be empty");
			log.debug("Exiting listAllEmployees service");

			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_ACCEPTABLE);
		}

		log.info("calling company service");
		cachet = employeeService.fetchAllEmployees(tenant_code);
		if (cachet.getStatus().equals(constants.getStatus_message_failure())) {
			log.debug("Exiting listAllEmployees service");
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_IMPLEMENTED);
		}
		if (cachet.getStatus().equals(constants.getStatus_message_exception())) {
			log.debug("Exiting listAllEmployees service");
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		log.info("Exiting listAllEmployees service");
		return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.OK);

	}

	/**
	 * service to delete employees
	 * 
	 * @param employee_ids - id's of employees
	 * 
	 * @return - {@value ResponseEntity<ResponseCachet>}
	 */
	@SuppressWarnings({ "rawtypes" })
	@RequestMapping(method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResponseCachet> deleteEmployees(@RequestParam("employee_ids") Set<String> employee_ids) {
		log.debug("Entering deleteEmployee method");

		ResponseCachet cachet = new ResponseCachet();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		if (CollectionUtils.isEmpty(employee_ids)) {
			log.error("Employee Ids can't be empty");
			cachet.setStatus(constants.getStatus_message_failure());
			cachet.setMessage("Employee Ids can't be empty");
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_ACCEPTABLE);
		}
		log.info("calling Employee Ids service");
		cachet = employeeService.deleteEmployees(employee_ids);
		if (cachet.getStatus().equals(constants.getStatus_message_failure())) {
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_IMPLEMENTED);
		} else if (cachet.getStatus().equals(constants.getStatus_message_exception())) {
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		log.info("Exiting Employee Ids service");
		return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.OK);

	}

	@SuppressWarnings("rawtypes")
	@GetMapping("/getEmployes")
	public ResponseEntity<ResponseCachet> findAllEmployeesByIsLogin(@RequestParam("tenant_code") String tenant_code) {
		log.info("Entering findAllEmployeesByIsLogin method");
		ResponseCachet cachet = new ResponseCachet<>();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		log.info("validating the request");
		if (StringUtils.isEmpty(tenant_code)) {
			log.info("request can't be empty");

			cachet.setStatus(constants.getStatus_message_failure());
			cachet.setMessage("request can't be empty");
			log.debug("Exiting listAllEmployees service");

			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_ACCEPTABLE);
		}
		log.info("calling company service");
		cachet = employeeService.getAllEmployeesByIsLogin(tenant_code);
		if (cachet.getStatus().equals(constants.getStatus_message_failure())) {
			log.debug("Exiting listAllEmployees service");
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_IMPLEMENTED);
		}
		if (cachet.getStatus().equals(constants.getStatus_message_exception())) {
			log.debug("Exiting listAllEmployees service");
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		log.info("Exiting listAllEmployees service");
		return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.OK);
	}

}
