package com.spaceinje.tenant.serviceImpl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.web.multipart.MultipartFile;

import com.spaceinje.tenant.EmployeeServiceClient;
import com.spaceinje.tenant.bean.EmployeeBean;
import com.spaceinje.tenant.bean.UserInfoBean;
import com.spaceinje.tenant.constants.TenantConstants;
import com.spaceinje.tenant.model.Employee;
import com.spaceinje.tenant.repository.EmployeeRepository;
import com.spaceinje.tenant.service.AWSService;
import com.spaceinje.tenant.service.EmployeeService;
import com.spaceinje.tenant.specification.CommonSpecs;
import com.spaceinje.tenant.util.ResponseCachet;

import brave.Tracer;

@Service
public class EmployeeServiceImpl implements EmployeeService {

	private static final Logger log = LoggerFactory.getLogger(EmployeeServiceImpl.class);

	@Autowired
	private EmployeeRepository employeeRepository;

	@Autowired
	private ModelMapper modelMapper;

	@Autowired
	private TenantConstants constants;

	@Autowired
	private Tracer tracer;

	@Autowired
	private AWSService awsService;
	
	@Autowired
	private EmployeeServiceClient employeeServiceClient;

	@Override
	public ResponseCachet<EmployeeBean> saveOrUpdateEmployee(EmployeeBean bean, MultipartFile file) {
		ResponseCachet<EmployeeBean> cachet = new ResponseCachet<>();
		modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		try {
			log.debug("check whether tenant consists of supplier with given shortcode");

			if (bean.getGenerateId() && StringUtils.isEmpty(bean.getId())) {
				bean.setEmployeeId(generateEmployeeId(bean));
			}
			final Employee employee = modelMapper.map(bean, Employee.class);
			employee.getAddress().setEmployee(employee);
			employee.getBankDetails().setEmployee(employee);
			log.info("uploading profile picture");
			if (file != null && !file.isEmpty()) {

				String image_url = "";
				try {
					image_url = awsService.uploadProfilePicToS3(file, bean.getTenantCode(),"Employee");
				} catch (Exception ex) {
					log.error("error occured while uploading profile picture" + ex.getMessage());
				}
				employee.setPhotoUrl(image_url);
			}
			if(employee.getLoginEnabled()) {
				UserInfoBean userInfo = new UserInfoBean();
				userInfo.setEmail(employee.getEmail());
				userInfo.setFirstName(employee.getFirstName());
				userInfo.setIsTenant(false);
				userInfo.setIsDeleted(false);
				userInfo.setLastName(employee.getLastName());
				userInfo.setMiddleName(employee.getMiddleName());
				userInfo.setMobile(employee.getMobile());
				userInfo.setUserName(employee.getMobile());
				userInfo.setPassword(constants.getEmployeeTemporaryPassword());
				userInfo.setTenantCode(employee.getTenantCode());
				employeeServiceClient.setUserInfoDetails(userInfo);
			}
			Employee savedEmployee = employeeRepository.save(employee);
			log.debug("successfully saved employee with id : {}", employee.getEmployeeId());
			cachet.setStatus(constants.getStatus_message_success());
			cachet.setMessage("Employee with id " + bean.getEmployeeId() + " saved successfully");
			cachet.setData(modelMapper.map(savedEmployee, EmployeeBean.class));
			return cachet;
		} catch (Exception e) {
			log.error("Exception while saving supplier: {}", e);
			cachet.setStatus(constants.getStatus_message_exception());
			cachet.setMessage(e.getMessage());
			return cachet;
		}
	}

	@Override
	public ResponseCachet<?> deleteEmployees(Set<String> ids) {
		ResponseCachet<?> cachet = new ResponseCachet<>();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		try {
			employeeRepository.deleteEmployees(ids);
			log.debug("deleted Employee(s) successfully");
			cachet.setStatus(constants.getStatus_message_success());
			cachet.setMessage("deleted Employee(s) successfully");
			return cachet;
		} catch (Exception e) {
			log.error("Exception while deleting Employee(s): {}", e);
			cachet.setStatus(constants.getStatus_message_exception());
			cachet.setMessage(e.getMessage());
			return cachet;
		}

	}

	@SuppressWarnings("unchecked")
	@Override
	public ResponseCachet<List<EmployeeBean>> fetchAllEmployees(String tenantCode) {
		ResponseCachet<List<EmployeeBean>> cachet = new ResponseCachet<>();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		try {
			List<Employee> list = employeeRepository.findAll(CommonSpecs.findByTenantCodeSpec(tenantCode));
			if (CollectionUtils.isEmpty(list)) {
				cachet.setStatus(constants.getStatus_message_success());
				cachet.setMessage("No Employee found");
				cachet.setData(new ArrayList<>());
				log.debug("No Employee found to the tenant");
				return cachet;
			}
			log.debug("Successfully retrieved all Employees");
			cachet.setStatus(constants.getStatus_message_success());
			cachet.setMessage("successfully retrieved Employees");
			cachet.setData(
					list.stream().map(ele -> modelMapper.map(ele, EmployeeBean.class)).collect(Collectors.toList()));
			return cachet;
		} catch (Exception e) {
			log.error("Exception while retrieving all Employees: {}", e);
			cachet.setStatus(constants.getStatus_message_exception());
			cachet.setMessage(e.getMessage());
			return cachet;
		}
	}

	private String generateEmployeeId(EmployeeBean bean) {

		Optional<Employee> emp = employeeRepository.findTopByTenantCodeOrderByCreatedDateDesc(bean.getTenantCode());

		String empId;
		if (!emp.isPresent()) {
			empId = "EMP" + "00001";
		} else {
			int id = Integer.valueOf(emp.get().getEmployeeId().replace("EMP", ""));
			empId = "EMP" + StringUtils.leftPad(String.valueOf(id + 1), 5, "0");

		}
		return empId;
	}

	@SuppressWarnings("unchecked")
	@Override
	public ResponseCachet<List<EmployeeBean>> getAllEmployeesByIsLogin(String tenantCode) {
		// TODO Auto-generated method stub
		ResponseCachet<List<EmployeeBean>> cachet = new ResponseCachet<>();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		try {
			List<Employee> list = employeeRepository.findAll(CommonSpecs.findByTenantCodeSpec(tenantCode).and(CommonSpecs.isLoginSpec()));
			if (CollectionUtils.isEmpty(list)) {
				cachet.setStatus(constants.getStatus_message_success());
				cachet.setMessage("No Employee found");
				cachet.setData(new ArrayList<>());
				log.debug("No Employee found to the tenant");
				return cachet;
			}
			log.debug("Successfully retrieved all Employees");
			cachet.setStatus(constants.getStatus_message_success());
			cachet.setMessage("successfully retrieved Employees");
			cachet.setData(
					list.stream().map(ele -> modelMapper.map(ele, EmployeeBean.class)).collect(Collectors.toList()));
			return cachet;
		} catch (Exception e) {
			log.error("Exception while retrieving all Employees: {}", e);
			cachet.setStatus(constants.getStatus_message_exception());
			cachet.setMessage(e.getMessage());
			return cachet;
		}

	}

}
