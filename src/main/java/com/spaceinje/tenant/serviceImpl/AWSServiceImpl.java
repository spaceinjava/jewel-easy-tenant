/**
 * 
 */
package com.spaceinje.tenant.serviceImpl;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.SdkClientException;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.DeleteObjectRequest;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.spaceinje.tenant.constants.TenantConstants;
import com.spaceinje.tenant.service.AWSService;

/**
 * @author Lakshmi Kiran
 *
 */
@Service("awsService")
public class AWSServiceImpl implements AWSService {

	private static final Logger log = LoggerFactory.getLogger(AWSServiceImpl.class);

	@Autowired
	private TenantConstants constants;

	@Autowired
	private AmazonS3 amazonS3;

	/**
	 * service to upload profile pic image to S3 bucket
	 * 
	 * @return url of the image
	 * 
	 * @throws IOException            - if the file was not found or unavailable
	 * 
	 * @throws AmazonServiceException - while uploading image to s3 bucket
	 */
	@Override
	public String uploadProfilePicToS3(MultipartFile multipartFile, String tenent_code, String sourceName)
			throws IOException, AmazonServiceException, SdkClientException {

		if (log.isDebugEnabled())
			log.debug("Entering uploadProfilePicToS3 service");

		log.info("converting multipart file to file");
		final File file = new File(multipartFile.getOriginalFilename());
		final FileOutputStream fileOutputStream = new FileOutputStream(file);
		fileOutputStream.write(multipartFile.getBytes());
		// closing output stream
		fileOutputStream.close();

		log.info("uploading file to S3 bucket");
		final String file_name = tenent_code + file.getName();
		String bucketName = constants.getAws_s3_bucket_name()+"/"+tenent_code+"/"+sourceName;
		final PutObjectRequest request = new PutObjectRequest(bucketName, file_name, file);
		request.withCannedAcl(CannedAccessControlList.PublicRead);
		amazonS3.putObject(request);

		// deleting the local image
		file.delete();
		return amazonS3.getUrl(bucketName, file_name).toExternalForm();
	}

	@Override
	public void deleteFile(String file_name) throws AmazonServiceException, SdkClientException {
		if (log.isDebugEnabled())
			log.debug("Entering deleteFile service");

		log.info("deleting file");
		file_name = file_name.replace(constants.getAws_s3_bucket_url(), "");
		System.out.println(file_name);
		final DeleteObjectRequest deleteObjectRequest = new DeleteObjectRequest(constants.getAws_s3_bucket_name(),
				file_name);
		amazonS3.deleteObject(deleteObjectRequest);
		log.info("deleted file successfully");
	}

}
