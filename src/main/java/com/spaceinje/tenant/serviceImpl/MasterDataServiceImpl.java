package com.spaceinje.tenant.serviceImpl;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.spaceinje.tenant.bean.base.BankBean;
import com.spaceinje.tenant.bean.base.PostalBean;
import com.spaceinje.tenant.constants.TenantConstants;
import com.spaceinje.tenant.model.base.Bank;
import com.spaceinje.tenant.model.base.Postal;
import com.spaceinje.tenant.repository.BankMasterRepo;
import com.spaceinje.tenant.repository.PostalMasterRepo;
import com.spaceinje.tenant.service.MasterDataService;
import com.spaceinje.tenant.util.ResponseCachet;

import brave.Tracer;

@Service
public class MasterDataServiceImpl implements MasterDataService {

	private static final Logger log = LoggerFactory.getLogger(MasterDataServiceImpl.class);

	@Autowired
	private BankMasterRepo bankMasterRepo;
	
	@Autowired
	private PostalMasterRepo postalMasterRepo;

	@Autowired
	private ModelMapper modelMapper;

	@Autowired
	private TenantConstants constants;

	@Autowired
	private Tracer tracer;

	@Override
	public ResponseCachet<BankBean> getBankDetailsByIfsc(String ifsc) {

		ResponseCachet<BankBean> cachet = new ResponseCachet<BankBean>();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		try {
			Optional<Bank> bank = bankMasterRepo.findById(ifsc);
			
			if (bank.isPresent()) {
				log.info("retrieved bank details using IFSC successfully");
				cachet.setStatus(constants.getStatus_message_success());
				cachet.setMessage("retrieved bank details using IFSC successfully");
				cachet.setData(modelMapper.map(bank.get(), BankBean.class));
				log.debug("Exiting getBankDetailsByIFSC service");
				return cachet;
			}
			log.info("No bank details found for given IFSC");
			cachet.setStatus(constants.getStatus_message_failure());
			cachet.setMessage("No bank details found for given IFSC");
			return cachet;
		} catch (Exception e) {
			log.error("Exception occured while retrieving bank details", e);
			cachet.setStatus(constants.getStatus_message_exception());
			cachet.setMessage("Exception occured while retrieving bank details");
			return cachet;
		}
	}
	
	@Override
	public ResponseCachet<List<PostalBean>> getPostalDetailsByPincode(String pincode) {
		
		ResponseCachet<List<PostalBean>> cachet = new ResponseCachet<List<PostalBean>>();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		try {
			Optional<List<Postal>> postal = postalMasterRepo.findByPincode(pincode);
			if (postal.isPresent()) {
				log.info("retrieved postal details using pincode successfully");
				cachet.setStatus(constants.getStatus_message_success());
				cachet.setMessage("retrieved bank details using pincode successfully");
				List<PostalBean> postalBeans = postal.get().stream().map(pos -> modelMapper.map(pos, PostalBean.class)).collect(Collectors.toList());
				postalBeans.forEach(bean -> bean.setMandal(bean.getMandal().replace(" Mandal", "")));
				postalBeans.forEach(bean -> bean.setVillage(bean.getVillage().replace(" B.O", "").replace(" S.O", "")));
				cachet.setData(postalBeans);
				log.debug("Exiting getPostalDetailsByPincode service");
				return cachet;
			}
			log.info("No postal details found for given pincode");
			cachet.setStatus(constants.getStatus_message_failure());
			cachet.setMessage("No postal details found for given pincode");
			return cachet;
		} catch (Exception e) {
			log.error("Exception occured while retrieving postal details", e);
			cachet.setStatus(constants.getStatus_message_exception());
			cachet.setMessage("Exception occured while retrieving postal details");
			return cachet;
		}
	}

}
