package com.spaceinje.tenant.serviceImpl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.web.multipart.MultipartFile;

import com.spaceinje.tenant.bean.SupplierBean;
import com.spaceinje.tenant.constants.TenantConstants;
import com.spaceinje.tenant.model.Supplier;
import com.spaceinje.tenant.repository.SupplierRepository;
import com.spaceinje.tenant.service.AWSService;
import com.spaceinje.tenant.service.SupplierService;
import com.spaceinje.tenant.specification.CommonSpecs;
import com.spaceinje.tenant.util.ResponseCachet;

import brave.Tracer;

@SuppressWarnings("unchecked")
@Service("supplierService")
public class SupplierServiceImpl implements SupplierService {

	private static final Logger log = LoggerFactory.getLogger(SupplierServiceImpl.class);

	@Autowired
	private ModelMapper modelMapper;

	@Autowired
	private SupplierRepository supplierRepository;

	@Autowired
	private TenantConstants constants;

	@Autowired
	private Tracer tracer;

	@Autowired
	private AWSService awsService;

	@Override
	public ResponseCachet<List<SupplierBean>> fetchAllSuppliers(String tenantCode) {
		ResponseCachet<List<SupplierBean>> cachet = new ResponseCachet<>();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		try {
			List<Supplier> list = supplierRepository.findAll(CommonSpecs.findByTenantCodeSpec(tenantCode));
			if (CollectionUtils.isEmpty(list)) {
				cachet.setStatus(constants.getStatus_message_success());
				cachet.setMessage("No Supplier found");
				cachet.setData(new ArrayList<>());
				log.debug("No Supplier found to the tenant");
				return cachet;
			}
			log.debug("Successfully retrieved all Supplier");
			cachet.setStatus(constants.getStatus_message_success());
			cachet.setMessage("successfully retrieved Supplier");
			cachet.setData(
					list.stream().map(ele -> modelMapper.map(ele, SupplierBean.class)).collect(Collectors.toList()));
			return cachet;
		} catch (Exception e) {
			log.error("Exception while retrieving all Supplier: {}", e);
			cachet.setStatus(constants.getStatus_message_exception());
			cachet.setMessage(e.getMessage());
			return cachet;
		}
	}

	@Override
	public ResponseCachet<?> deleteSuppliers(Set<String> supplierIds) {
		ResponseCachet<?> cachet = new ResponseCachet<>();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		try {
			supplierRepository.deleteSuppliers(supplierIds);
			log.debug("deleted supplier(s) successfully");
			cachet.setStatus(constants.getStatus_message_success());
			cachet.setMessage("deleted supplier(s) successfully");
			return cachet;
		} catch (Exception e) {
			log.error("Exception while deleting suppliers: {}", e);
			cachet.setStatus(constants.getStatus_message_exception());
			cachet.setMessage(e.getMessage());
			return cachet;
		}
	}

	@Override
	public ResponseCachet<?> saveOrUpdateSupplier(SupplierBean bean, MultipartFile file) {
		ResponseCachet<SupplierBean> cachet = new ResponseCachet<>();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		try {
			log.debug("check whether tenant consists of supplier with given shortcode and gst");
			Optional<Supplier> retrievedSupplier = supplierRepository
					.findOne(CommonSpecs.findByTenantAndShortCodeGSTSpec(bean.getTenantCode(), bean.getShortCode(), bean.getGstNum()));
			if (retrievedSupplier.isPresent() && !retrievedSupplier.get().getId().equals(bean.getId())) {
				cachet.setStatus(constants.getStatus_message_failure());
				cachet.setMessage("short code and GST already exists");
				log.debug("Short code already exists to the tenant {}", bean.getTenantCode());
				return cachet;
			}

			log.info("uploading profile picture");
			final Supplier supplier = modelMapper.map(bean, Supplier.class);

			if (file != null && !file.isEmpty()) {

				String image_url = "";
				try {
					image_url = awsService.uploadProfilePicToS3(file, bean.getTenantCode(), "Supplier");
				} catch (Exception ex) {
					log.error("error occured while uploading profile picture" + ex.getMessage());
				}
				supplier.setLogoUrl(image_url);
			}
			supplier.getAddress().setSupplier(supplier);
			supplier.getSupplierDetails().stream().forEach(ele -> ele.setSupplier(supplier));
			supplier.getBankDetails().stream().forEach(ele -> ele.setSupplier(supplier));
			supplierRepository.save(supplier);
			log.debug("successfully saved supplier with name : {}", supplier.getPartyName());
			cachet.setStatus(constants.getStatus_message_success());
			cachet.setMessage("supplier with shortcode " + bean.getShortCode() + " saved successfully");
			cachet.setData(modelMapper.map(supplier, SupplierBean.class));
			return cachet;
		} catch (Exception e) {
			log.error("Exception while saving supplier: {}", e);
			cachet.setStatus(constants.getStatus_message_exception());
			cachet.setMessage(e.getMessage());
			return cachet;
		}

	}

}
