package com.spaceinje.tenant.serviceImpl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.spaceinje.tenant.bean.DepartmentBean;
import com.spaceinje.tenant.constants.TenantConstants;
import com.spaceinje.tenant.model.Department;
import com.spaceinje.tenant.repository.DepartmentRepository;
import com.spaceinje.tenant.service.DepartmentService;
import com.spaceinje.tenant.specification.CommonSpecs;
import com.spaceinje.tenant.util.ResponseCachet;

import brave.Tracer;

@SuppressWarnings("unchecked")
@Service("departmentService")
public class DepartmentServiceImpl implements DepartmentService {

	private static final Logger log = LoggerFactory.getLogger(DepartmentServiceImpl.class);

	@Autowired
	private ModelMapper modelMapper;

	@Autowired
	private DepartmentRepository departmentRepository;

	@Autowired
	private TenantConstants constants;

	@Autowired
	private Tracer tracer;

	@Override
	public ResponseCachet<?> saveOrUpdateDepartment(DepartmentBean bean) {
		ResponseCachet<DepartmentBean> cachet = new ResponseCachet<>();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		try {
			log.debug("check whether tenant consists of department with given shortcode");
			Optional<Department> retrievedDepartment = departmentRepository
					.findOne(CommonSpecs.findByTenantAndShortCodeSpec(bean.getTenantCode(), bean.getShortCode()));
			if (retrievedDepartment.isPresent() && !retrievedDepartment.get().getId().equals(bean.getId())) {
				cachet.setStatus(constants.getStatus_message_failure());
				cachet.setMessage("short code already exists");
				log.debug("Short code already exists to the tenant {}", bean.getTenantCode());
				return cachet;
			}
			Department department = modelMapper.map(bean, Department.class);
			department = departmentRepository.save(department);
			log.debug("successfully saved department with name : {}", department.getDepartmentName());
			cachet.setStatus(constants.getStatus_message_success());
			cachet.setMessage("department with shortcode " + bean.getShortCode() + " saved successfully");
			cachet.setData(modelMapper.map(department, DepartmentBean.class));
			return cachet;
		} catch (Exception e) {
			log.error("Exception while saving department: {}", e);
			cachet.setStatus(constants.getStatus_message_exception());
			cachet.setMessage(e.getMessage());
			return cachet;
		}
	}

	@Override
	public ResponseCachet<List<DepartmentBean>> fetchAllDepartments(String tenantCode) {
		ResponseCachet<List<DepartmentBean>> cachet = new ResponseCachet<>();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		try {
			List<Department> list = departmentRepository.findAll(CommonSpecs.findByTenantCodeSpec(tenantCode));
			if (CollectionUtils.isEmpty(list)) {
				cachet.setStatus(constants.getStatus_message_success());
				cachet.setMessage("No department found");
				cachet.setData(new ArrayList<>());
				log.debug("No department found to the tenant");
				return cachet;
			}
			log.debug("Successfully retrieved all departments");
			cachet.setStatus(constants.getStatus_message_success());
			cachet.setMessage("successfully retrieved departments");
			cachet.setData(
					list.stream().map(ele -> modelMapper.map(ele, DepartmentBean.class)).collect(Collectors.toList()));
			return cachet;
		} catch (Exception e) {
			log.error("Exception while retrieving all departments: {}", e);
			cachet.setStatus(constants.getStatus_message_exception());
			cachet.setMessage(e.getMessage());
			return cachet;
		}
	}

	@Override
	public ResponseCachet<?> deleteDepartments(Set<String> departmentIds) {
		ResponseCachet<?> cachet = new ResponseCachet<>();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		try {
			if (departmentRepository.departmentHasOrphans(departmentIds)) {
				log.debug("department(s) has children data associated with given Id's");
				cachet.setStatus(constants.getStatus_message_failure());
				cachet.setMessage("Department(s) can't be deleted, as they are already in use.!");
				return cachet;
			}
			departmentRepository.deleteDepartments(departmentIds);
			log.debug("deleted department(s) successfully");
			cachet.setStatus(constants.getStatus_message_success());
			cachet.setMessage("deleted department(s) successfully");
			return cachet;
		} catch (Exception e) {
			log.error("Exception while deleting departments: {}", e);
			cachet.setStatus(constants.getStatus_message_exception());
			cachet.setMessage(e.getMessage());
			return cachet;
		}
	}

}
