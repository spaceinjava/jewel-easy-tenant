package com.spaceinje.tenant.serviceImpl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.spaceinje.tenant.bean.DesignationBean;
import com.spaceinje.tenant.constants.TenantConstants;
import com.spaceinje.tenant.converter.DesignationConverter;
import com.spaceinje.tenant.model.Designation;
import com.spaceinje.tenant.repository.DesignationRepository;
import com.spaceinje.tenant.service.DesignationService;
import com.spaceinje.tenant.specification.CommonSpecs;
import com.spaceinje.tenant.util.ResponseCachet;

import brave.Tracer;

@SuppressWarnings("unchecked")
@Service("designationService")
public class DesignationServiceImpl implements DesignationService {

	private static final Logger log = LoggerFactory.getLogger(DesignationServiceImpl.class);

	@Autowired
	private DesignationConverter designationConverter;

	@Autowired
	private DesignationRepository designationRepository;

	@Autowired
	private TenantConstants constants;

	@Autowired
	private Tracer tracer;

	@Override
	public ResponseCachet<?> saveOrUpdateDesignation(DesignationBean bean) {
		ResponseCachet<DesignationBean> cachet = new ResponseCachet<>();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		try {
			log.debug("check whether tenant consists of designation with given shortcode");
			Optional<Designation> retrievedDesignation = designationRepository
					.findOne(CommonSpecs.findByTenantAndShortCodeSpec(bean.getTenantCode(), bean.getShortCode()));
			if (retrievedDesignation.isPresent() && !retrievedDesignation.get().getId().equals(bean.getId())) {
				cachet.setStatus(constants.getStatus_message_failure());
				cachet.setMessage("short code already exists");
				log.debug("Short code already exists to the tenant {}", bean.getTenantCode());
				return cachet;
			}
			Designation designation = designationConverter.beanToEntity(bean);
			designation = designationRepository.save(designation);
			log.debug("successfully saved designation with name : {}", designation.getDesignationName());
			cachet.setStatus(constants.getStatus_message_success());
			cachet.setMessage("designation with shortcode " + bean.getShortCode() + " saved successfully");
			cachet.setData(designationConverter.entityToBean(designation));
			return cachet;
		} catch (Exception e) {
			log.error("Exception while saving designation: {}", e);
			cachet.setStatus(constants.getStatus_message_exception());
			cachet.setMessage(e.getMessage());
			return cachet;
		}
	}

	@Override
	public ResponseCachet<List<DesignationBean>> fetchAllDesignations(String tenantCode) {
		ResponseCachet<List<DesignationBean>> cachet = new ResponseCachet<>();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		try {
			List<Designation> list = designationRepository.findAll(CommonSpecs.findByTenantCodeSpec(tenantCode));
			if (CollectionUtils.isEmpty(list)) {
				cachet.setStatus(constants.getStatus_message_success());
				cachet.setMessage("No designation found");
				cachet.setData(new ArrayList<>());
				log.debug("No designation found to the tenant");
				return cachet;
			}
			log.debug("Successfully retrieved all designations");
			cachet.setStatus(constants.getStatus_message_success());
			cachet.setMessage("successfully retrieved designations");
			cachet.setData(
					list.stream().map(ele -> designationConverter.entityToBean(ele)).collect(Collectors.toList()));
			return cachet;
		} catch (Exception e) {
			log.error("Exception while retrieving all designations: {}", e);
			cachet.setStatus(constants.getStatus_message_exception());
			cachet.setMessage(e.getMessage());
			return cachet;
		}
	}

	@Override
	public ResponseCachet<?> deleteDesignations(Set<String> designationIds) {
		ResponseCachet<?> cachet = new ResponseCachet<>();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		try {
			designationRepository.deleteDesignations(designationIds);
			log.debug("deleted designation(s) successfully");
			cachet.setStatus(constants.getStatus_message_success());
			cachet.setMessage("deleted designation(s) successfully");
			return cachet;
		} catch (Exception e) {
			log.error("Exception while deleting designations: {}", e);
			cachet.setStatus(constants.getStatus_message_exception());
			cachet.setMessage(e.getMessage());
			return cachet;
		}
	}

}
