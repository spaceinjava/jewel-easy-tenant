package com.spaceinje.tenant.serviceImpl;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.spaceinje.tenant.bean.BranchBean;
import com.spaceinje.tenant.constants.TenantConstants;
import com.spaceinje.tenant.model.Branch;
import com.spaceinje.tenant.repository.BranchRepository;
import com.spaceinje.tenant.service.BranchService;
import com.spaceinje.tenant.specification.CommonSpecs;
import com.spaceinje.tenant.util.ResponseCachet;

import brave.Tracer;

@Service
public class BranchServiceImpl implements BranchService {

	private static final Logger log = LoggerFactory.getLogger(BranchServiceImpl.class);

	@Autowired
	private BranchRepository branchRepository;

	@Autowired
	private ModelMapper modelMapper;

	@Autowired
	private TenantConstants constants;

	@Autowired
	private Tracer tracer;

	@Override
	public ResponseCachet<BranchBean> saveBranch(BranchBean bean) {
		ResponseCachet<BranchBean> cachet = new ResponseCachet<>();
		modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		try {
			log.debug("check whether tenant consists of branch with given shortcode");
			final Branch branch = modelMapper.map(bean, Branch.class);

			branch.getAddress().setBranch(branch);
			branch.getBankDetails().stream().forEach(bankDetail -> bankDetail.setBranch(branch));
			Branch savedBranch = branchRepository.save(branch);
			log.debug("successfully saved Branch with short code : {}", bean.getShortCode());
			cachet.setStatus(constants.getStatus_message_success());
			cachet.setMessage("Branch with short code " + bean.getShortCode() + " saved successfully");
			cachet.setData(modelMapper.map(savedBranch, BranchBean.class));
			return cachet;
		} catch (Exception e) {
			log.error("Exception while saving Branch: {}", e);
			cachet.setStatus(constants.getStatus_message_exception());
			cachet.setMessage(e.getMessage());
			return cachet;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public ResponseCachet<List<BranchBean>> fetchAllBranches(String tenantCode) {
		ResponseCachet<List<BranchBean>> cachet = new ResponseCachet<>();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		try {
			List<Branch> list = branchRepository.findAll(CommonSpecs.findByTenantCodeSpec(tenantCode));
			if (CollectionUtils.isEmpty(list)) {
				cachet.setStatus(constants.getStatus_message_success());
				cachet.setMessage("No Branch found");
				cachet.setData(new ArrayList<>());
				log.debug("No Branch found to the tenant");
				return cachet;
			}
			log.debug("Successfully retrieved all Branches");
			cachet.setStatus(constants.getStatus_message_success());
			cachet.setMessage("successfully retrieved Branches");
			cachet.setData(
					list.stream().map(ele -> modelMapper.map(ele, BranchBean.class)).collect(Collectors.toList()));
			return cachet;
		} catch (Exception e) {
			log.error("Exception while retrieving all Branches: {}", e);
			cachet.setStatus(constants.getStatus_message_exception());
			cachet.setMessage(e.getMessage());
			return cachet;
		}
	}

	@Override
	public ResponseCachet<?> deleteBranches(Set<String> ids) {
		ResponseCachet<?> cachet = new ResponseCachet<>();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		try {
			if (branchRepository.existsByIdInAndIsHeadBranchTrue(ids)) {
				cachet.setStatus(constants.getStatus_message_failure());
				cachet.setMessage("Can't delete head branch");
				return cachet;
			}
			branchRepository.deleteBranches(ids);
			log.debug("deleted Branch(s) successfully");
			cachet.setStatus(constants.getStatus_message_success());
			cachet.setMessage("deleted Branch(s) successfully");
			return cachet;
		} catch (Exception e) {
			log.error("Exception while deleting Branch(s): {}", e);
			cachet.setStatus(constants.getStatus_message_exception());
			cachet.setMessage(e.getMessage());
			return cachet;
		}

	}

}
