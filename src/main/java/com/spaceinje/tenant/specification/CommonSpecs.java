package com.spaceinje.tenant.specification;

import org.springframework.data.jpa.domain.Specification;

@SuppressWarnings({ "rawtypes", "unchecked" })
public class CommonSpecs {

	/**
	 * <b>creates criteria to retrieve based on isDeleted false
	 * 
	 * @return
	 */
	private static Specification isDeletedSpec() {
		return (root, query, criteriaBuilder) -> criteriaBuilder.equal(root.get("isDeleted"), false);
	}

	/**
	 * return criteria to retrieve based on given tenantCode
	 * 
	 * @param tenantCode
	 * @return
	 */
	private static Specification tenantCodeSpec(String tenantCode) {
		return (root, query, criteriaBuilder) -> criteriaBuilder.equal(root.get("tenantCode"), tenantCode);
	}

	/**
	 * return criteria to retrieve based on given shortCode
	 * 
	 * @param shortCode
	 * @return
	 */
	private static Specification shortCodeSpec(String shortCode) {
		return (root, query, criteriaBuilder) -> criteriaBuilder.equal(root.get("shortCode"), shortCode);
	}

	/**
	 * return criteria to retrieve based on given shortCode
	 * 
	 * @param shortCode
	 * @return
	 */
	private static Specification gstSpec(String gstNum) {
		return (root, query, criteriaBuilder) -> criteriaBuilder.equal(root.get("gstNum"), gstNum);
	}
	
	/**
	 * <b>creates criteria to retrieve based on isDeleted false and tenantCode</b>
	 * 
	 * @param tenantCode
	 * @return
	 */
	public static Specification findByTenantCodeSpec(String tenantCode) {
		return Specification.where(tenantCodeSpec(tenantCode)).and(isDeletedSpec());
	}

	/**
	 * <b>creates criteria to retrieve based on isDeleted false and shortCode</b>
	 * 
	 * @param shortCode
	 * @return
	 */
	public static Specification findByShortCodeSpec(String shortCode) {
		return Specification.where(shortCodeSpec(shortCode)).and(isDeletedSpec());
	}

	public static Specification findByGSTSpec(String gstNum) {
		return Specification.where(gstSpec(gstNum)).and(isDeletedSpec());
	}
	/**
	 * <b>creates criteria to retrieve based on isDeleted false, tenantCode and
	 * shortCode</b>
	 * 
	 * @param tenantCode
	 * @param shortCode
	 * @return
	 */
	public static Specification findByTenantAndShortCodeSpec(String tenantCode, String shortCode) {
		return Specification.where(findByTenantCodeSpec(tenantCode)).and(findByShortCodeSpec(shortCode))
				.and(isDeletedSpec());
	}

	public static Specification findByTenantAndShortCodeGSTSpec(String tenantCode, String shortCode, String gstNum) {
		return Specification.where(findByTenantCodeSpec(tenantCode)).and(findByShortCodeSpec(shortCode))
				.and(isDeletedSpec().and(findByGSTSpec(gstNum)));
	}
	
	
	/**
	 * return criteria to retrieve based on given loginEnabled
	 * 
	 * @param loginEnabled
	 * @return
	 */
	public static Specification isLoginSpec() {
		return (root, query, criteriaBuilder) -> criteriaBuilder.equal(root.get("loginEnabled"), true);
	}
}
