FROM java:8
WORKDIR /
ADD ./target/je*.jar jecreations.jar
EXPOSE 80
CMD ["java", "-jar", "jecreations.jar"]